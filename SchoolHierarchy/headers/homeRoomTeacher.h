#ifndef HomeRoomTeacher_h
#define HomeRoomTeacher_h
#include "./dynamicMemoryStringArray.h"
#include "./teachingStaff.h"

/** @brief Class representing a home-room teacher.
 *
 * The home-room teachers are in charge of guiding a particular class
 * or standard for a specific period of time.
 * They teach more than one number of subjects to the specific class.
 * In addition to this, they undertake the task of coordinating various
 * events of the class at periodic intervals of time,
 * prepare the grade sheets of the individual students,
 * develops the quarterly and annual report cards of the students,
 * and communicate with the parents regarding the development in the students.
 */

class HomeRoomTeacher : public TeachingStaff
{
public:
    HomeRoomTeacher(const char* = DEFAULT_NAME,
                    const char* = DEFAULT_ADDRESS,
                    const int& = DEFAULT_BIRTHDAY_YEAR,
                    const char* = DEFAULT_TELEPHONE_NUMBER,
                    const char* = DEFAULT_EMAIL,
                    const double& = DEFAULT_MONTLY_SALARY,
                    const int& = DEFAULT_EXPERIENCE,
                    const char* = DEFAULT_SCHOOL_RANK,
                    const char* = DEFAULT_GRADE_CLASS_TAUGHT);
    HomeRoomTeacher(const HomeRoomTeacher&);
    virtual ~HomeRoomTeacher();
    HomeRoomTeacher& operator=(const HomeRoomTeacher&);

    DynamicMemoryStringArray getSubjectsTaught() const;
    char* getGradeClassTaught() const;

    virtual void addSubjectTaught(const char*);
    virtual void removeSubjectTaught(const char*);

    virtual void addGradeClassTaught(const char*);
    virtual void removeGradeClassTaught(const char*);

private:
    DynamicMemoryStringArray subjectsTaught;
    char* gradeClassTaught;

    void copyHomeRoomTeacher(const HomeRoomTeacher&);
    void deleteHomeRoomTeacher();

    void setGradeClassTaught(const char*);
};

#endif  // HomeRoomTeacher_h
