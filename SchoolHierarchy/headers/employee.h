#ifndef Employee_h
#define Employee_h
#include "./communityMember.h"

/** @brief Class representing an employee.
 *
 * An employee contributes labor and expertise to an endeavor of an employer or
 * of a person conducting a business and is usually
 * hired to perform specific duties which are packaged into a job. In a
 * corporate context, an employee is a person who is hired to provide services
 * to a company on a regular basis in exchange for compensation and who does not
 * provide these services as part of an independent business.
 */

class Employee : public CommunityMember
{
public:
    Employee(const char* = DEFAULT_NAME,
             const char* = DEFAULT_ADDRESS,
             const int& = DEFAULT_BIRTHDAY_YEAR,
             const char* = DEFAULT_TELEPHONE_NUMBER,
             const char* = DEFAULT_EMAIL,
             const double& = DEFAULT_MONTLY_SALARY,
             const int& = DEFAULT_EXPERIENCE);
    Employee(const Employee&);
    virtual ~Employee();
    Employee& operator=(const Employee&);

    int getExpereince() const;
    double getMonthlySalary() const;

    void setExperience(const int&);

    /** @brief Virtual functions which increase and decrese the employee's
     *   salary properly depending on the position of the employee.
     */
    virtual void increaseMonthlySalary() = 0;
    virtual void decreaseMonthlySalary() = 0;

protected:
    double monthlySalary;

private:
    int experience;

    void copyEmployee(const Employee&);
    void deleteEmployee();

    void setMonthlySalary(const double&);
};

#endif  // Employee_h
