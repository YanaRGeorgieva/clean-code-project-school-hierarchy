#ifndef CommunityMember_h
#define CommunityMember_h
#include "./constants.h"
#include <algorithm>
#include <cassert>

/** @brief representing a person from the community.
 *
 * Just a simple representation of some of the most important data an individual
 * owns, so to identify himself in the community.
 */

class CommunityMember
{
public:
    CommunityMember(const char* = DEFAULT_NAME,
                    const char* = DEFAULT_ADDRESS,
                    const int& = DEFAULT_BIRTHDAY_YEAR,
                    const char* = DEFAULT_TELEPHONE_NUMBER,
                    const char* = DEFAULT_EMAIL);
    CommunityMember(const CommunityMember&);
    virtual ~CommunityMember();
    CommunityMember& operator=(const CommunityMember&);

    char* getName() const;
    char* getAddress() const;
    int getBirthYear() const;
    char* getTelephoneNumber() const;
    char* getEmail() const;

    int calculateAge() const;

    bool operator==(const CommunityMember&) const;

    void setName(const char*);
    void setAddress(const char*);
    void setBirthYear(const int&);
    void setTelephoneNumber(const char*);
    void setEmail(const char*);

protected:
    void copyDynamicString(char*&, const char*);
    bool isItemInBag(const bag&, const char*) const;
    bool isItemInBag(const responsabilityToMoneyBoost&, const char*) const;
    double retrieveMoneyInMapping(const responsabilityToMoneyBoost&,
                                  const char*) const;

private:
    char* name;
    char* address;
    int birthYear;
    char* telephoneNumber;
    char* email;

    void copyCommunityMember(const CommunityMember&);
    void deleteCommunityMember();

    int getCurrentYear() const;
};

#endif  // CommunityMember_h
