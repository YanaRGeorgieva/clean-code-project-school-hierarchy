#ifndef Student_h
#define Student_h
#include "./communityMember.h"
#include "./dynamicMemoryStringArray.h"

/** @brief Class representing a student.
 *
 * A student is primarily a person enrolled in a school or other educational
 * institution who attends classes in a course to attain the appropriate level
 * of mastery of a subject under the guidance of an instructor and who devotes
 * time outside class to do whatever activities the instructor assigns that are
 * necessary either for class preparation or to submit evidence of progress
 * towards that mastery. In the broader sense, a student is anyone who applies
 * themselves to the intensive intellectual engagement with some matter
 * necessary to master it as part of some practical affair in which such mastery
 * is basic or decisive.
 */

class Student : public CommunityMember
{
public:
    Student(const char* = DEFAULT_NAME,
            const char* = DEFAULT_ADDRESS,
            const int& = DEFAULT_BIRTHDAY_YEAR,
            const char* = DEFAULT_TELEPHONE_NUMBER,
            const char* = DEFAULT_EMAIL,
            const double& = DEFAULT_AVERAGE_MARK,
            const int& = DEFAULT_CLASS_NUMBER,
            const char* = DEFAULT_GRADE_CLASS);
    Student(const Student&);
    virtual ~Student();
    Student& operator=(const Student&);

    double getAverageMark() const;
    DynamicMemoryStringArray getProfiledSubjects() const;
    int getClassNumber() const;
    char* getGradeClass() const;

    void setAverageMark(const double&);
    void setClassNumber(const int&);
    void setGradeClass(const char*);
    void addProfiledSubject(const char*);
    void removeProfiledSubject(const char*);

private:
    double averageMark;
    int classNumber;
    char* gradeClass;
    DynamicMemoryStringArray profiledSubjects;

    void copyStudent(const Student&);
    void deleteStudent();
};

#endif  // Student_h
