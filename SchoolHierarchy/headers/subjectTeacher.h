#ifndef SubjectTeacher_h
#define SubjectTeacher_h
#include "./dynamicMemoryStringArray.h"
#include "./teachingStaff.h"

/** @brief Class representing a subject teacher.
 *
 * The subject teachers teach a particular subject to different classes.
 * They communicate with the home-room teachers at regular intervals to discuss
 * about any improvement in the teaching strategies.
 */

class SubjectTeacher : public TeachingStaff
{
public:
    SubjectTeacher(const char* = DEFAULT_NAME,
                   const char* = DEFAULT_ADDRESS,
                   const int& = DEFAULT_BIRTHDAY_YEAR,
                   const char* = DEFAULT_TELEPHONE_NUMBER,
                   const char* = DEFAULT_EMAIL,
                   const double& = DEFAULT_MONTLY_SALARY,
                   const int& = DEFAULT_EXPERIENCE,
                   const char* = DEFAULT_SCHOOL_RANK,
                   const char* = DEFAULT_SUBJECT_TAUGHT);
    SubjectTeacher(const SubjectTeacher&);
    virtual ~SubjectTeacher();
    SubjectTeacher& operator=(const SubjectTeacher&);

    char* getSubjectTaught() const;
    DynamicMemoryStringArray getGradeClassesTaught() const;

    virtual void addSubjectTaught(const char*);
    virtual void removeSubjectTaught(const char*);

    virtual void addGradeClassTaught(const char*);
    virtual void removeGradeClassTaught(const char*);

private:
    char* subjectTaught;
    DynamicMemoryStringArray gradeClassesTaught;

    void copySubjectTeacher(const SubjectTeacher&);
    void deleteSubjectTeacher();

    void setSubjectTaught(const char*);
};

#endif  // SubjectTeacher_h
