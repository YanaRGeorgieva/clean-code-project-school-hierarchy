#ifndef Alumni_h
#define Alumni_h
#include "./communityMember.h"
#include "./dynamicMemoryStringArray.h"

/** @brief representing a person from the administration (nonteachers) staff.
 *
 * An alumnus or an alumna of a
 * college, university, or other school is a former student who has either
 * attended or graduated in some fashion from the institution.
 */

class Alumni : public CommunityMember
{
public:
    Alumni(const char* = DEFAULT_NAME,
           const char* = DEFAULT_ADDRESS,
           const int& = DEFAULT_BIRTHDAY_YEAR,
           const char* = DEFAULT_TELEPHONE_NUMBER,
           const char* = DEFAULT_EMAIL,
           const double& = DEFAULT_DIPLOMA_MARK);
    Alumni(const Alumni&);
    virtual ~Alumni();
    Alumni& operator=(const Alumni&);

    double getDiplomaMark() const;
    DynamicMemoryStringArray getProfiledSubjects() const;

    void setDiplomaMark(const double&);
    void addProfiledSubject(const char*);
    void removeProfiledSubject(const char*);

private:
    double diplomaMark;
    DynamicMemoryStringArray profiledSubjects;

    void copyAlumni(const Alumni&);
    void deleteAlumni();
};

#endif  // Alumni_h
