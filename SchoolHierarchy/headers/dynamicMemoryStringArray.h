#ifndef DynamicMemoryStringArray_h
#define DynamicMemoryStringArray_h
#include "./constants.h"

/** @brief Class representing a dynamic array of char*.
 *
 */

class DynamicMemoryStringArray
{
public:
    DynamicMemoryStringArray(const int& = DEFAULT_ARRAY_CAPACITY);
    DynamicMemoryStringArray(const DynamicMemoryStringArray&);
    ~DynamicMemoryStringArray();
    DynamicMemoryStringArray& operator=(const DynamicMemoryStringArray&);

    int getSize() const;
    char*& operator[](const int&);

    void addElement(const char*);
    void removeElement(const char*);

private:
    char** array;
    int size;
    int capacity;

    void copyDynamicMemoryStringArray(const DynamicMemoryStringArray&);
    void deleteDynamicMemoryStringArray();

    int findElementIndex(const char*) const;

    void resize(const int&);
    void fixPointersAfterRemoveElement(const int&);

    void copyDynamicString(char*&, const char*);
};

#endif  // DynamicMemoryStringArray_h
