#ifndef TeachingStaff_h
#define TeachingStaff_h
#include "./employee.h"

/** @brief Class representing a person from the teachers staff.
 *
 * A teacher (also called a school teacher or, in some contexts, an
 * educator) is a person who helps others to acquire knowledge, competence or
 * virtue.
 */

class TeachingStaff : public Employee
{
public:
    TeachingStaff(const char* = DEFAULT_NAME,
                  const char* = DEFAULT_ADDRESS,
                  const int& = DEFAULT_BIRTHDAY_YEAR,
                  const char* = DEFAULT_TELEPHONE_NUMBER,
                  const char* = DEFAULT_EMAIL,
                  const double& = DEFAULT_MONTLY_SALARY,
                  const int& = DEFAULT_EXPERIENCE,
                  const char* = DEFAULT_SCHOOL_RANK);
    TeachingStaff(const TeachingStaff&);
    virtual ~TeachingStaff();
    TeachingStaff& operator=(const TeachingStaff&);

    char* getSchoolRank() const;

    void setSchoolRank(const char*);

    void increaseMonthlySalary();
    void decreaseMonthlySalary();

    virtual void addSubjectTaught(const char*) = 0;
    virtual void removeSubjectTaught(const char*) = 0;

    virtual void addGradeClassTaught(const char*) = 0;
    virtual void removeGradeClassTaught(const char*) = 0;

private:
    char* schoolRank;

    void copyTeachingStaff(const TeachingStaff&);
    void deleteTeachingStaff();
};

#endif  // TeachingStaff_h
