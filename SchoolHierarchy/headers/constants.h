#ifndef Constants_h
#define Constants_h

#include <map>
#include <unordered_set>

typedef std::map<const char*, double> responsabilityToMoneyBoost;

typedef std::unordered_set<const char*> bag;

extern const bag gradeClassesInSchool, subjectsInSchool;

extern const responsabilityToMoneyBoost nonTeachnicalStaffJobs, schoolRanks;

extern const int DEFAULT_BIRTHDAY_YEAR, DEFAULT_EXPERIENCE,
    DEFAULT_CLASS_NUMBER, DEFAULT_FOUNDATION_YEAR, DEFAULT_ARRAY_CAPACITY,
    DEFAULT_START_YEAR;

extern const double DEFAULT_MONTLY_SALARY, DEFAULT_DIPLOMA_MARK,
    DEFAULT_AVERAGE_MARK;

extern const char *DEFAULT_NAME, *DEFAULT_ADDRESS, *DEFAULT_TELEPHONE_NUMBER,
    *DEFAULT_EMAIL, *DEFAULT_SCHOOL_RANK, *DEFAULT_GRADE_CLASS_TAUGHT,
    *DEFAULT_GRADE_CLASS, *DEFAULT_SUBJECT_TAUGHT, *DEFAULT_JOB,
    *DEFAULT_SCHOOL_NAME;
#endif  // Constants_h