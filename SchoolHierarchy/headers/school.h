#ifndef School_h
#define School_h
#include "./alumni.h"
#include "./homeRoomTeacher.h"
#include "./nonTeachingStaff.h"
#include "./student.h"
#include "./subjectTeacher.h"
#include <vector>

/** @brief Class representing a school.
 *
 * A school is an educational institution designed to provide learning spaces
 * and learning environments for the teaching of students (or "pupils") under
 * the direction of teachers. Most countries have systems of formal education,
 * which is commonly compulsory. In these systems, students
 * progress through a series of schools. The names for these schools vary by
 * country but generally include primary school for young children and secondary
 * school for teenagers who have completed primary education. An institution
 * where higher education is taught, is commonly called a university college or
 * university, but these higher education institutions are usually not
 * compulsory.
 */

class School
{
public:
    School(const char* = DEFAULT_SCHOOL_NAME,
           const int& = DEFAULT_FOUNDATION_YEAR);
    School(const School&);
    ~School();
    School& operator=(const School&);

    char* getName() const;
    int getFoundationYear() const;
    std::vector<TeachingStaff*> getTeachers() const;
    std::vector<NonTeachingStaff> getNonTeachers() const;
    std::vector<Student> getStudents() const;
    std::vector<Alumni> getAlumnus() const;

    void setName(const char*);
    void setFoundationYear(const int&);
    void setTeachers(const std::vector<TeachingStaff*>&);
    void setNonTeachers(const std::vector<NonTeachingStaff>&);
    void setStudents(const std::vector<Student>&);
    void setAlumnus(const std::vector<Alumni>&);

    void addTeacher(HomeRoomTeacher);
    void addTeacher(SubjectTeacher);
    void removeTeacher(const char*);

    void addNonTeacher(const NonTeachingStaff&);
    void removeNonTeacher(const char*);

    void addStudent(const Student&);
    void removeStudent(const char*);

    void addAlumni(const Alumni&);
    void removeAlumni(const char*);

private:
    char* name;
    int foundationYear;
    std::vector<TeachingStaff*> teachers;
    std::vector<NonTeachingStaff> nonTeachers;
    std::vector<Student> students;
    std::vector<Alumni> alumnus;

    void copySchool(const School&);
    void deleteSchool();

    void copyDynamicString(char*&, const char*);
};

#endif  // School_h