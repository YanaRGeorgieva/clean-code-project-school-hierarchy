#ifndef NonTeachingStaff_h
#define NonTeachingStaff_h
#include "./employee.h"

/** @brief Class representing a person from the administration
 * (nonteachers) staff.
 *
 * The administration staff in the school management hierarchy performs
 * various non-teaching duties of the school.
 */

class NonTeachingStaff : public Employee
{
public:
    NonTeachingStaff(const char* = DEFAULT_NAME,
                     const char* = DEFAULT_ADDRESS,
                     const int& = DEFAULT_BIRTHDAY_YEAR,
                     const char* = DEFAULT_TELEPHONE_NUMBER,
                     const char* = DEFAULT_EMAIL,
                     const double& = DEFAULT_MONTLY_SALARY,
                     const int& = DEFAULT_EXPERIENCE,
                     const char* = DEFAULT_JOB);
    NonTeachingStaff(const NonTeachingStaff&);
    virtual ~NonTeachingStaff();
    NonTeachingStaff& operator=(const NonTeachingStaff&);

    char* getJob() const;

    void setJob(const char*);

    virtual void increaseMonthlySalary();
    virtual void decreaseMonthlySalary();

private:
    char* job;

    void copyNonTeachingStaff(const NonTeachingStaff&);
    void deleteNonTeachingStaff();
};

#endif  // NonTeachingStaff_h
