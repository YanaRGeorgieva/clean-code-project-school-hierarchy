#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "./../headers/alumni.h"
#include "./../headers/communityMember.h"
#include "./../headers/constants.h"
#include "./../headers/employee.h"
#include "./../headers/homeRoomTeacher.h"
#include "./../headers/nonTeachingStaff.h"
#include "./../headers/school.h"
#include "./../headers/student.h"
#include "./../headers/subjectTeacher.h"
#include "./../headers/teachingStaff.h"
#include "doctest.h"
#include <algorithm>
#include <cmath>
#include <cstring>
#include <memory>

const double EPSILON = 0.0001;

bool approximatelyEqual(const float& a, const float& b, const float& epsilon)
{
    return fabs(a - b) <= ((fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * epsilon);
}

TEST_CASE("Testing commutity member class.")
{
    SUBCASE("Testing default constructor.")
    {
        CommunityMember person;
        CHECK(strcmp(person.getName(), DEFAULT_NAME) == 0);
        CHECK(person.getBirthYear() == DEFAULT_BIRTHDAY_YEAR);
        CHECK(strcmp(person.getEmail(), DEFAULT_EMAIL) == 0);
        CHECK(strcmp(person.getAddress(), DEFAULT_ADDRESS) == 0);
        CHECK(strcmp(person.getTelephoneNumber(), DEFAULT_TELEPHONE_NUMBER) ==
              0);
    }

    SUBCASE("Testing parametric constructor.")
    {
        CommunityMember person("Adre", "Sofia", 1997, "0895623362",
                               "adreD@gmail.com");
        CHECK(strcmp(person.getName(), "Adre") == 0);
        CHECK(person.getBirthYear() == 1997);
        CHECK(strcmp(person.getEmail(), "adreD@gmail.com") == 0);
        CHECK(strcmp(person.getAddress(), "Sofia") == 0);
        CHECK(strcmp(person.getTelephoneNumber(), "0895623362") == 0);
    }

    SUBCASE("Testing setters.")
    {
        CommunityMember person("Adre", "Sofia", 1997, "0895623362",
                               "adreD@gmail.com");
        person.setName("Michel");
        person.setBirthYear(2000);
        person.setEmail("micha@abv.bg");
        person.setAddress("Varna");
        person.setTelephoneNumber("0895644856");

        CHECK(strcmp(person.getName(), "Michel") == 0);
        CHECK(person.getBirthYear() == 2000);
        CHECK(strcmp(person.getEmail(), "micha@abv.bg") == 0);
        CHECK(strcmp(person.getAddress(), "Varna") == 0);
        CHECK(strcmp(person.getTelephoneNumber(), "0895644856") == 0);
    }

    SUBCASE("Testing copy constructor.")
    {
        CommunityMember person("Adre", "Sofia", 1997, "0895623362",
                               "adreD@gmail.com");
        CommunityMember otherPerson(person);

        CHECK(strcmp(otherPerson.getName(), "Adre") == 0);
        CHECK(otherPerson.getBirthYear() == 1997);
        CHECK(strcmp(otherPerson.getEmail(), "adreD@gmail.com") == 0);
        CHECK(strcmp(otherPerson.getAddress(), "Sofia") == 0);
        CHECK(strcmp(otherPerson.getTelephoneNumber(), "0895623362") == 0);
    }

    SUBCASE("Testing operator=.")
    {
        CommunityMember person("Adre", "Sofia", 1997, "0895623362",
                               "adreD@gmail.com");
        CommunityMember otherPerson;
        otherPerson = person;

        CHECK(strcmp(otherPerson.getName(), "Adre") == 0);
        CHECK(otherPerson.getBirthYear() == 1997);
        CHECK(strcmp(otherPerson.getEmail(), "adreD@gmail.com") == 0);
        CHECK(strcmp(otherPerson.getAddress(), "Sofia") == 0);
        CHECK(strcmp(otherPerson.getTelephoneNumber(), "0895623362") == 0);
    }
}

TEST_CASE("Testing student class.")
{
    SUBCASE("Testing default constructor.")
    {
        Student person;
        CHECK(strcmp(person.getName(), DEFAULT_NAME) == 0);
        CHECK(person.getBirthYear() == DEFAULT_BIRTHDAY_YEAR);
        CHECK(strcmp(person.getEmail(), DEFAULT_EMAIL) == 0);
        CHECK(strcmp(person.getAddress(), DEFAULT_ADDRESS) == 0);
        CHECK(strcmp(person.getTelephoneNumber(), DEFAULT_TELEPHONE_NUMBER) ==
              0);
        CHECK(approximatelyEqual(person.getAverageMark(), DEFAULT_AVERAGE_MARK,
                                 EPSILON));
        CHECK(person.getClassNumber() == DEFAULT_CLASS_NUMBER);
        CHECK(strcmp(person.getGradeClass(), DEFAULT_GRADE_CLASS) == 0);
    }
    SUBCASE("Testing parametric constructor.")
    {
        Student person("Adre", "Sofia", 1997, "0895623362", "adreD@gmail.com",
                       5.0, 5, "1B");
        CHECK(strcmp(person.getName(), "Adre") == 0);
        CHECK(person.getBirthYear() == 1997);
        CHECK(strcmp(person.getEmail(), "adreD@gmail.com") == 0);
        CHECK(strcmp(person.getAddress(), "Sofia") == 0);
        CHECK(strcmp(person.getTelephoneNumber(), "0895623362") == 0);
        CHECK(approximatelyEqual(person.getAverageMark(), 5.0, EPSILON));
        CHECK(person.getClassNumber() == 5);
        CHECK(strcmp(person.getGradeClass(), "1B") == 0);
    }

    SUBCASE("Testing setters.")
    {
        Student person;
        person.setName("Michel");
        person.setBirthYear(2000);
        person.setEmail("micha@abv.bg");
        person.setAddress("Varna");
        person.setTelephoneNumber("0895644856");
        person.setAverageMark(6.0);
        person.setClassNumber(9);
        person.setGradeClass("10B");

        CHECK(strcmp(person.getName(), "Michel") == 0);
        CHECK(person.getBirthYear() == 2000);
        CHECK(strcmp(person.getEmail(), "micha@abv.bg") == 0);
        CHECK(strcmp(person.getAddress(), "Varna") == 0);
        CHECK(strcmp(person.getTelephoneNumber(), "0895644856") == 0);
        CHECK(approximatelyEqual(person.getAverageMark(), 6.0, EPSILON));
        CHECK(person.getClassNumber() == 9);
        CHECK(strcmp(person.getGradeClass(), "10B") == 0);
    }

    SUBCASE("Testing copy constructor.")
    {
        Student person("Adre", "Sofia", 1997, "0895623362", "adreD@gmail.com",
                       5.0, 5, "10B");
        Student otherPerson(person);

        CHECK(strcmp(otherPerson.getName(), "Adre") == 0);
        CHECK(otherPerson.getBirthYear() == 1997);
        CHECK(strcmp(otherPerson.getEmail(), "adreD@gmail.com") == 0);
        CHECK(strcmp(otherPerson.getAddress(), "Sofia") == 0);
        CHECK(strcmp(otherPerson.getTelephoneNumber(), "0895623362") == 0);
        CHECK(approximatelyEqual(otherPerson.getAverageMark(), 5.0, EPSILON));
        CHECK(otherPerson.getClassNumber() == 5);
        CHECK(strcmp(otherPerson.getGradeClass(), "10B") == 0);
    }

    SUBCASE("Testing operator=.")
    {
        Student person("Adre", "Sofia", 1997, "0895623362", "adreD@gmail.com",
                       5.0, 5, "10B");
        Student otherPerson;
        otherPerson = person;

        CHECK(strcmp(otherPerson.getName(), "Adre") == 0);
        CHECK(otherPerson.getBirthYear() == 1997);
        CHECK(strcmp(otherPerson.getEmail(), "adreD@gmail.com") == 0);
        CHECK(strcmp(otherPerson.getAddress(), "Sofia") == 0);
        CHECK(strcmp(otherPerson.getTelephoneNumber(), "0895623362") == 0);
        CHECK(approximatelyEqual(otherPerson.getAverageMark(), 5.0, EPSILON));
        CHECK(otherPerson.getClassNumber() == 5);
        CHECK(strcmp(otherPerson.getGradeClass(), "10B") == 0);
    }

    SUBCASE("Testing profiled subjects array.")
    {
        Student person("Adre", "Sofia", 1997, "0895623362", "adreD@gmail.com",
                       5.0, 5, "10B");

        person.addProfiledSubject("Mathematics");
        person.addProfiledSubject("Geography");
        person.addProfiledSubject("Biology");
        auto arr = person.getProfiledSubjects();
        int size = arr.getSize();

        for (int i = 0; i < size; i++)
        {
            auto element = arr[i];
            auto find =
                std::find_if(subjectsInSchool.begin(), subjectsInSchool.end(),
                             [&element](const char* somesubject) {
                                 return strcmp(element, somesubject) == 0;
                             });
            CHECK(find != subjectsInSchool.end());
        }
        person.removeProfiledSubject("Biology");
        arr = person.getProfiledSubjects();
        size = arr.getSize();
        for (int i = 0; i < size; i++)
        {
            auto element = arr[i];
            auto find =
                std::find_if(subjectsInSchool.begin(), subjectsInSchool.end(),
                             [&element](const char* somesubject) {
                                 return strcmp(element, somesubject) == 0;
                             });
            CHECK(find != subjectsInSchool.end());
        }
        Student john = person;
        arr = john.getProfiledSubjects();
        size = arr.getSize();
        for (int i = 0; i < size; i++)
        {
            auto element = arr[i];
            auto find =
                std::find_if(subjectsInSchool.begin(), subjectsInSchool.end(),
                             [&element](const char* somesubject) {
                                 return strcmp(element, somesubject) == 0;
                             });
            CHECK(find != subjectsInSchool.end());
        }
    }
}

TEST_CASE("Testing alumni class.")
{
    SUBCASE("Testing default constructor.")
    {
        Alumni person;
        CHECK(strcmp(person.getName(), DEFAULT_NAME) == 0);
        CHECK(person.getBirthYear() == DEFAULT_BIRTHDAY_YEAR);
        CHECK(strcmp(person.getEmail(), DEFAULT_EMAIL) == 0);
        CHECK(strcmp(person.getAddress(), DEFAULT_ADDRESS) == 0);
        CHECK(strcmp(person.getTelephoneNumber(), DEFAULT_TELEPHONE_NUMBER) ==
              0);
        CHECK(approximatelyEqual(person.getDiplomaMark(), DEFAULT_DIPLOMA_MARK,
                                 EPSILON));
    }
    SUBCASE("Testing parametric constructor.")
    {
        Alumni person("Adre", "Sofia", 1997, "0895623362", "adreD@gmail.com",
                      5.0);
        CHECK(strcmp(person.getName(), "Adre") == 0);
        CHECK(person.getBirthYear() == 1997);
        CHECK(strcmp(person.getEmail(), "adreD@gmail.com") == 0);
        CHECK(strcmp(person.getAddress(), "Sofia") == 0);
        CHECK(strcmp(person.getTelephoneNumber(), "0895623362") == 0);
        CHECK(approximatelyEqual(person.getDiplomaMark(), 5.0, EPSILON));
    }

    SUBCASE("Testing setters.")
    {
        Alumni person;
        person.setName("Michel");
        person.setBirthYear(2000);
        person.setEmail("micha@abv.bg");
        person.setAddress("Varna");
        person.setTelephoneNumber("0895644856");
        person.setDiplomaMark(6.0);

        CHECK(strcmp(person.getName(), "Michel") == 0);
        CHECK(person.getBirthYear() == 2000);
        CHECK(strcmp(person.getEmail(), "micha@abv.bg") == 0);
        CHECK(strcmp(person.getAddress(), "Varna") == 0);
        CHECK(strcmp(person.getTelephoneNumber(), "0895644856") == 0);
        CHECK(approximatelyEqual(person.getDiplomaMark(), 6.0, EPSILON));
    }

    SUBCASE("Testing copy constructor.")
    {
        Alumni person("Adre", "Sofia", 1997, "0895623362", "adreD@gmail.com",
                      5.0);
        Alumni otherPerson(person);

        CHECK(strcmp(otherPerson.getName(), "Adre") == 0);
        CHECK(otherPerson.getBirthYear() == 1997);
        CHECK(strcmp(otherPerson.getEmail(), "adreD@gmail.com") == 0);
        CHECK(strcmp(otherPerson.getAddress(), "Sofia") == 0);
        CHECK(strcmp(otherPerson.getTelephoneNumber(), "0895623362") == 0);
        CHECK(approximatelyEqual(otherPerson.getDiplomaMark(), 5.0, EPSILON));
    }

    SUBCASE("Testing operator=.")
    {
        Alumni person("Adre", "Sofia", 1997, "0895623362", "adreD@gmail.com",
                      5.0);
        Alumni otherPerson;
        otherPerson = person;

        CHECK(strcmp(otherPerson.getName(), "Adre") == 0);
        CHECK(otherPerson.getBirthYear() == 1997);
        CHECK(strcmp(otherPerson.getEmail(), "adreD@gmail.com") == 0);
        CHECK(strcmp(otherPerson.getAddress(), "Sofia") == 0);
        CHECK(strcmp(otherPerson.getTelephoneNumber(), "0895623362") == 0);
        CHECK(approximatelyEqual(otherPerson.getDiplomaMark(), 5.0, EPSILON));
    }

    SUBCASE("Testing profiled subjects array.")
    {
        Alumni person("Adre", "Sofia", 1997, "0895623362", "adreD@gmail.com",
                      5.0);

        person.addProfiledSubject("Mathematics");
        person.addProfiledSubject("Geography");
        person.addProfiledSubject("Biology");
        auto arr = person.getProfiledSubjects();
        int size = arr.getSize();

        for (int i = 0; i < size; i++)
        {
            auto element = arr[i];
            auto find =
                std::find_if(subjectsInSchool.begin(), subjectsInSchool.end(),
                             [&element](const char* somesubject) {
                                 return strcmp(element, somesubject) == 0;
                             });
            CHECK(find != subjectsInSchool.end());
        }
        person.removeProfiledSubject("Biology");
        arr = person.getProfiledSubjects();
        size = arr.getSize();
        for (int i = 0; i < size; i++)
        {
            auto element = arr[i];
            auto find =
                std::find_if(subjectsInSchool.begin(), subjectsInSchool.end(),
                             [&element](const char* somesubject) {
                                 return strcmp(element, somesubject) == 0;
                             });
            CHECK(find != subjectsInSchool.end());
        }

        Alumni mary = person;
        arr = mary.getProfiledSubjects();
        size = arr.getSize();
        for (int i = 0; i < size; i++)
        {
            auto element = arr[i];
            auto find =
                std::find_if(subjectsInSchool.begin(), subjectsInSchool.end(),
                             [&element](const char* somesubject) {
                                 return strcmp(element, somesubject) == 0;
                             });
            CHECK(find != subjectsInSchool.end());
        }
    }
}

TEST_CASE("Testing non teaching staff class.")
{
    SUBCASE("Testing default constructor.")
    {
        NonTeachingStaff person;
        CHECK(strcmp(person.getName(), DEFAULT_NAME) == 0);
        CHECK(person.getBirthYear() == DEFAULT_BIRTHDAY_YEAR);
        CHECK(strcmp(person.getEmail(), DEFAULT_EMAIL) == 0);
        CHECK(strcmp(person.getAddress(), DEFAULT_ADDRESS) == 0);
        CHECK(strcmp(person.getTelephoneNumber(), DEFAULT_TELEPHONE_NUMBER) ==
              0);
        CHECK(approximatelyEqual(person.getMonthlySalary(),
                                 DEFAULT_MONTLY_SALARY, EPSILON));
        CHECK(person.getExpereince() == DEFAULT_EXPERIENCE);
        CHECK(strcmp(person.getJob(), DEFAULT_JOB) == 0);
    }
    SUBCASE("Testing parametric constructor.")
    {
        NonTeachingStaff person("Adre", "Sofia", 1997, "0895623362",
                                "adreD@gmail.com", 700.0, 4, "Secretary");
        CHECK(strcmp(person.getName(), "Adre") == 0);
        CHECK(person.getBirthYear() == 1997);
        CHECK(strcmp(person.getEmail(), "adreD@gmail.com") == 0);
        CHECK(strcmp(person.getAddress(), "Sofia") == 0);
        CHECK(strcmp(person.getTelephoneNumber(), "0895623362") == 0);
        CHECK(approximatelyEqual(person.getMonthlySalary(), 700.0, EPSILON));
        CHECK(person.getExpereince() == 4);
        CHECK(strcmp(person.getJob(), "Secretary") == 0);
    }

    SUBCASE("Testing setters.")
    {
        NonTeachingStaff person;
        person.setName("Michel");
        person.setBirthYear(2000);
        person.setEmail("micha@abv.bg");
        person.setAddress("Varna");
        person.setTelephoneNumber("0895644856");
        person.setExperience(6);
        person.setJob("Secretary");

        CHECK(strcmp(person.getName(), "Michel") == 0);
        CHECK(person.getBirthYear() == 2000);
        CHECK(strcmp(person.getEmail(), "micha@abv.bg") == 0);
        CHECK(strcmp(person.getAddress(), "Varna") == 0);
        CHECK(strcmp(person.getTelephoneNumber(), "0895644856") == 0);
        CHECK(person.getExpereince() == 6);
        CHECK(strcmp(person.getJob(), "Secretary") == 0);
    }

    SUBCASE("Testing copy constructor.")
    {
        NonTeachingStaff person("Adre", "Sofia", 1997, "0895623362",
                                "adreD@gmail.com", 700.0, 4, "Secretary");
        NonTeachingStaff otherPerson(person);

        CHECK(strcmp(otherPerson.getName(), "Adre") == 0);
        CHECK(otherPerson.getBirthYear() == 1997);
        CHECK(strcmp(otherPerson.getEmail(), "adreD@gmail.com") == 0);
        CHECK(strcmp(otherPerson.getAddress(), "Sofia") == 0);
        CHECK(strcmp(otherPerson.getTelephoneNumber(), "0895623362") == 0);
        CHECK(
            approximatelyEqual(otherPerson.getMonthlySalary(), 700.0, EPSILON));
        CHECK(otherPerson.getExpereince() == 4);
        CHECK(strcmp(otherPerson.getJob(), "Secretary") == 0);
    }

    SUBCASE("Testing operator=.")
    {
        NonTeachingStaff person("Adre", "Sofia", 1997, "0895623362",
                                "adreD@gmail.com", 700.0, 4, "Secretary");
        NonTeachingStaff otherPerson;
        otherPerson = person;

        CHECK(strcmp(otherPerson.getName(), "Adre") == 0);
        CHECK(otherPerson.getBirthYear() == 1997);
        CHECK(strcmp(otherPerson.getEmail(), "adreD@gmail.com") == 0);
        CHECK(strcmp(otherPerson.getAddress(), "Sofia") == 0);
        CHECK(strcmp(otherPerson.getTelephoneNumber(), "0895623362") == 0);
        CHECK(
            approximatelyEqual(otherPerson.getMonthlySalary(), 700.0, EPSILON));
        CHECK(otherPerson.getExpereince() == 4);
        CHECK(strcmp(otherPerson.getJob(), "Secretary") == 0);
    }

    SUBCASE("Testing salary operations.")
    {
        NonTeachingStaff person("Adre", "Sofia", 1997, "0895623362",
                                "adreD@gmail.com", 700.0, 4, "Secretary");
        person.increaseMonthlySalary();
        CHECK(approximatelyEqual(person.getMonthlySalary(), 950.0, EPSILON));
        person.decreaseMonthlySalary();
        CHECK(approximatelyEqual(person.getMonthlySalary(), 700.0, EPSILON));
    }
}

TEST_CASE("Testing home-room teacher class.")
{
    SUBCASE("Testing default constructor.")
    {
        HomeRoomTeacher person;
        CHECK(strcmp(person.getName(), DEFAULT_NAME) == 0);
        CHECK(person.getBirthYear() == DEFAULT_BIRTHDAY_YEAR);
        CHECK(strcmp(person.getEmail(), DEFAULT_EMAIL) == 0);
        CHECK(strcmp(person.getAddress(), DEFAULT_ADDRESS) == 0);
        CHECK(strcmp(person.getTelephoneNumber(), DEFAULT_TELEPHONE_NUMBER) ==
              0);
        CHECK(approximatelyEqual(person.getMonthlySalary(),
                                 DEFAULT_MONTLY_SALARY, EPSILON));
        CHECK(person.getExpereince() == DEFAULT_EXPERIENCE);
        CHECK(strcmp(person.getSchoolRank(), DEFAULT_SCHOOL_RANK) == 0);
        CHECK(strcmp(person.getGradeClassTaught(),
                     DEFAULT_GRADE_CLASS_TAUGHT) == 0);
    }
    SUBCASE("Testing parametric constructor.")
    {
        HomeRoomTeacher person("Adre", "Sofia", 1997, "0895623362",
                               "adreD@gmail.com", 700.0, 4, "Dean", "2B");
        CHECK(strcmp(person.getName(), "Adre") == 0);
        CHECK(person.getBirthYear() == 1997);
        CHECK(strcmp(person.getEmail(), "adreD@gmail.com") == 0);
        CHECK(strcmp(person.getAddress(), "Sofia") == 0);
        CHECK(strcmp(person.getTelephoneNumber(), "0895623362") == 0);
        CHECK(approximatelyEqual(person.getMonthlySalary(), 700.0, EPSILON));
        CHECK(person.getExpereince() == 4);
        CHECK(strcmp(person.getSchoolRank(), "Dean") == 0);
        CHECK(strcmp(person.getGradeClassTaught(), "2B") == 0);
    }

    SUBCASE("Testing setters.")
    {
        HomeRoomTeacher person;
        person.setName("Michel");
        person.setBirthYear(2000);
        person.setEmail("micha@abv.bg");
        person.setAddress("Varna");
        person.setTelephoneNumber("0895644856");
        person.setExperience(6);
        person.setSchoolRank("Dean");
        person.addGradeClassTaught("5D");

        CHECK(strcmp(person.getName(), "Michel") == 0);
        CHECK(person.getBirthYear() == 2000);
        CHECK(strcmp(person.getEmail(), "micha@abv.bg") == 0);
        CHECK(strcmp(person.getAddress(), "Varna") == 0);
        CHECK(strcmp(person.getTelephoneNumber(), "0895644856") == 0);
        CHECK(person.getExpereince() == 6);
        CHECK(strcmp(person.getSchoolRank(), "Dean") == 0);
        CHECK(strcmp(person.getGradeClassTaught(), "5D") == 0);
    }

    SUBCASE("Testing copy constructor.")
    {
        HomeRoomTeacher person("Adre", "Sofia", 1997, "0895623362",
                               "adreD@gmail.com", 700.0, 4, "Dean", "2B");
        HomeRoomTeacher otherPerson(person);

        CHECK(strcmp(otherPerson.getName(), "Adre") == 0);
        CHECK(otherPerson.getBirthYear() == 1997);
        CHECK(strcmp(otherPerson.getEmail(), "adreD@gmail.com") == 0);
        CHECK(strcmp(otherPerson.getAddress(), "Sofia") == 0);
        CHECK(strcmp(otherPerson.getTelephoneNumber(), "0895623362") == 0);
        CHECK(
            approximatelyEqual(otherPerson.getMonthlySalary(), 700.0, EPSILON));
        CHECK(otherPerson.getExpereince() == 4);
        CHECK(strcmp(otherPerson.getSchoolRank(), "Dean") == 0);
        CHECK(strcmp(otherPerson.getGradeClassTaught(), "2B") == 0);
    }

    SUBCASE("Testing operator=.")
    {
        HomeRoomTeacher person("Adre", "Sofia", 1997, "0895623362",
                               "adreD@gmail.com", 700.0, 4, "Dean", "2B");
        HomeRoomTeacher otherPerson;
        otherPerson = person;

        CHECK(strcmp(otherPerson.getName(), "Adre") == 0);
        CHECK(otherPerson.getBirthYear() == 1997);
        CHECK(strcmp(otherPerson.getEmail(), "adreD@gmail.com") == 0);
        CHECK(strcmp(otherPerson.getAddress(), "Sofia") == 0);
        CHECK(strcmp(otherPerson.getTelephoneNumber(), "0895623362") == 0);
        CHECK(
            approximatelyEqual(otherPerson.getMonthlySalary(), 700.0, EPSILON));
        CHECK(otherPerson.getExpereince() == 4);
        CHECK(strcmp(otherPerson.getSchoolRank(), "Dean") == 0);
        CHECK(strcmp(otherPerson.getGradeClassTaught(), "2B") == 0);
    }

    SUBCASE("Testing salary operations.")
    {
        HomeRoomTeacher person("Adre", "Sofia", 1997, "0895623362",
                               "adreD@gmail.com", 700.0, 4, "Dean", "2B");
        person.increaseMonthlySalary();
        CHECK(approximatelyEqual(person.getMonthlySalary(), 1200.0, EPSILON));
        person.decreaseMonthlySalary();
        CHECK(approximatelyEqual(person.getMonthlySalary(), 700.0, EPSILON));
    }

    SUBCASE("Testing subjects taught.")
    {
        HomeRoomTeacher person("Adre", "Sofia", 1997, "0895623362",
                               "adreD@gmail.com", 700.0, 4, "Dean", "2B");
        person.addSubjectTaught("Mathematics");
        person.addSubjectTaught("Geography");
        person.addSubjectTaught("Biology");
        auto arr = person.getSubjectsTaught();
        int size = arr.getSize();

        for (int i = 0; i < size; i++)
        {
            auto element = arr[i];
            auto find =
                std::find_if(subjectsInSchool.begin(), subjectsInSchool.end(),
                             [&element](const char* somesubject) {
                                 return strcmp(element, somesubject) == 0;
                             });
            CHECK(find != subjectsInSchool.end());
        }
        person.removeSubjectTaught("Biology");
        arr = person.getSubjectsTaught();
        size = arr.getSize();
        for (int i = 0; i < size; i++)
        {
            auto element = arr[i];
            auto find =
                std::find_if(subjectsInSchool.begin(), subjectsInSchool.end(),
                             [&element](const char* somesubject) {
                                 return strcmp(element, somesubject) == 0;
                             });
            CHECK(find != subjectsInSchool.end());
        }

        HomeRoomTeacher mary = person;
        arr = mary.getSubjectsTaught();
        size = arr.getSize();
        for (int i = 0; i < size; i++)
        {
            auto element = arr[i];
            auto find =
                std::find_if(subjectsInSchool.begin(), subjectsInSchool.end(),
                             [&element](const char* somesubject) {
                                 return strcmp(element, somesubject) == 0;
                             });
            CHECK(find != subjectsInSchool.end());
        }
    }
}

TEST_CASE("Testing subject teacher class.")
{
    SUBCASE("Testing default constructor.")
    {
        SubjectTeacher person;
        CHECK(strcmp(person.getName(), DEFAULT_NAME) == 0);
        CHECK(person.getBirthYear() == DEFAULT_BIRTHDAY_YEAR);
        CHECK(strcmp(person.getEmail(), DEFAULT_EMAIL) == 0);
        CHECK(strcmp(person.getAddress(), DEFAULT_ADDRESS) == 0);
        CHECK(strcmp(person.getTelephoneNumber(), DEFAULT_TELEPHONE_NUMBER) ==
              0);
        CHECK(approximatelyEqual(person.getMonthlySalary(),
                                 DEFAULT_MONTLY_SALARY, EPSILON));
        CHECK(person.getExpereince() == DEFAULT_EXPERIENCE);
        CHECK(strcmp(person.getSchoolRank(), DEFAULT_SCHOOL_RANK) == 0);
        CHECK(strcmp(person.getSubjectTaught(), DEFAULT_SUBJECT_TAUGHT) == 0);
    }
    SUBCASE("Testing parametric constructor.")
    {
        SubjectTeacher person("Adre", "Sofia", 1997, "0895623362",
                              "adreD@gmail.com", 700.0, 4, "Dean", "Geography");
        CHECK(strcmp(person.getName(), "Adre") == 0);
        CHECK(person.getBirthYear() == 1997);
        CHECK(strcmp(person.getEmail(), "adreD@gmail.com") == 0);
        CHECK(strcmp(person.getAddress(), "Sofia") == 0);
        CHECK(strcmp(person.getTelephoneNumber(), "0895623362") == 0);
        CHECK(approximatelyEqual(person.getMonthlySalary(), 700.0, EPSILON));
        CHECK(person.getExpereince() == 4);
        CHECK(strcmp(person.getSchoolRank(), "Dean") == 0);
        CHECK(strcmp(person.getSubjectTaught(), "Geography") == 0);
    }

    SUBCASE("Testing setters.")
    {
        SubjectTeacher person;
        person.setName("Michel");
        person.setBirthYear(2000);
        person.setEmail("micha@abv.bg");
        person.setAddress("Varna");
        person.setTelephoneNumber("0895644856");
        person.setExperience(6);
        person.setSchoolRank("Dean");
        person.addSubjectTaught("Mathematics");

        CHECK(strcmp(person.getName(), "Michel") == 0);
        CHECK(person.getBirthYear() == 2000);
        CHECK(strcmp(person.getEmail(), "micha@abv.bg") == 0);
        CHECK(strcmp(person.getAddress(), "Varna") == 0);
        CHECK(strcmp(person.getTelephoneNumber(), "0895644856") == 0);
        CHECK(person.getExpereince() == 6);
        CHECK(strcmp(person.getSchoolRank(), "Dean") == 0);
        CHECK(strcmp(person.getSubjectTaught(), "Mathematics") == 0);
    }

    SUBCASE("Testing copy constructor.")
    {
        SubjectTeacher person("Adre", "Sofia", 1997, "0895623362",
                              "adreD@gmail.com", 700.0, 4, "Dean", "Geography");
        SubjectTeacher otherPerson(person);

        CHECK(strcmp(otherPerson.getName(), "Adre") == 0);
        CHECK(otherPerson.getBirthYear() == 1997);
        CHECK(strcmp(otherPerson.getEmail(), "adreD@gmail.com") == 0);
        CHECK(strcmp(otherPerson.getAddress(), "Sofia") == 0);
        CHECK(strcmp(otherPerson.getTelephoneNumber(), "0895623362") == 0);
        CHECK(
            approximatelyEqual(otherPerson.getMonthlySalary(), 700.0, EPSILON));
        CHECK(otherPerson.getExpereince() == 4);
        CHECK(strcmp(otherPerson.getSchoolRank(), "Dean") == 0);
        CHECK(strcmp(otherPerson.getSubjectTaught(), "Geography") == 0);
    }

    SUBCASE("Testing operator=.")
    {
        SubjectTeacher person("Adre", "Sofia", 1997, "0895623362",
                              "adreD@gmail.com", 700.0, 4, "Dean", "Geography");
        SubjectTeacher otherPerson;
        otherPerson = person;

        CHECK(strcmp(otherPerson.getName(), "Adre") == 0);
        CHECK(otherPerson.getBirthYear() == 1997);
        CHECK(strcmp(otherPerson.getEmail(), "adreD@gmail.com") == 0);
        CHECK(strcmp(otherPerson.getAddress(), "Sofia") == 0);
        CHECK(strcmp(otherPerson.getTelephoneNumber(), "0895623362") == 0);
        CHECK(
            approximatelyEqual(otherPerson.getMonthlySalary(), 700.0, EPSILON));
        CHECK(otherPerson.getExpereince() == 4);
        CHECK(strcmp(otherPerson.getSchoolRank(), "Dean") == 0);
        CHECK(strcmp(otherPerson.getSubjectTaught(), "Geography") == 0);
    }

    SUBCASE("Testing salary operations.")
    {
        SubjectTeacher person("Adre", "Sofia", 1997, "0895623362",
                              "adreD@gmail.com", 700.0, 4, "Dean", "Geography");
        person.increaseMonthlySalary();
        CHECK(approximatelyEqual(person.getMonthlySalary(), 1200.0, EPSILON));
        person.decreaseMonthlySalary();
        CHECK(approximatelyEqual(person.getMonthlySalary(), 700.0, EPSILON));
    }

    SUBCASE("Testing grade classes taught.")
    {
        SubjectTeacher person("Adre", "Sofia", 1997, "0895623362",
                              "adreD@gmail.com", 700.0, 4, "Dean", "Geography");
        person.addGradeClassTaught("1C");
        person.addGradeClassTaught("6C");
        person.addGradeClassTaught("8D");
        auto arr = person.getGradeClassesTaught();
        int size = arr.getSize();

        for (int i = 0; i < size; i++)
        {
            auto element = arr[i];
            auto find = std::find_if(
                gradeClassesInSchool.begin(), gradeClassesInSchool.end(),
                [&element](const char* somesubject) {
                    return strcmp(element, somesubject) == 0;
                });
            CHECK(find != gradeClassesInSchool.end());
        }
        person.removeGradeClassTaught("8D");
        arr = person.getGradeClassesTaught();
        size = arr.getSize();
        for (int i = 0; i < size; i++)
        {
            auto element = arr[i];
            auto find = std::find_if(
                gradeClassesInSchool.begin(), gradeClassesInSchool.end(),
                [&element](const char* somesubject) {
                    return strcmp(element, somesubject) == 0;
                });
            CHECK(find != gradeClassesInSchool.end());
        }

        SubjectTeacher mary = person;
        arr = mary.getGradeClassesTaught();
        size = arr.getSize();
        for (int i = 0; i < size; i++)
        {
            auto element = arr[i];
            auto find = std::find_if(
                gradeClassesInSchool.begin(), gradeClassesInSchool.end(),
                [&element](const char* somesubject) {
                    return strcmp(element, somesubject) == 0;
                });
            CHECK(find != gradeClassesInSchool.end());
        }
    }
}

TEST_CASE("Testing school class.")
{
    SUBCASE("Testing default constructor.")
    {
        School williamGradston;
        CHECK(strcmp(williamGradston.getName(), DEFAULT_SCHOOL_NAME) == 0);
        CHECK(williamGradston.getFoundationYear() == DEFAULT_FOUNDATION_YEAR);
    }

    SUBCASE("Testing parametric constructor.")
    {
        School fpms("First private mathematical school", 2005);
        CHECK(strcmp(fpms.getName(), "First private mathematical school") == 0);
        CHECK(fpms.getFoundationYear() == 2005);
    }

    SUBCASE("Testing setters constructor.")
    {
        School fpms;
        fpms.setName("First private mathematical school");
        fpms.setFoundationYear(2005);

        CHECK(strcmp(fpms.getName(), "First private mathematical school") == 0);
        CHECK(fpms.getFoundationYear() == 2005);

        std::vector<Student> vSt = {Student("Adre", "Sofia", 1997, "0895623362",
                                            "adreD@gmail.com", 5.0, 5, "1B"),
                                    Student()};

        std::vector<Alumni> vAl = {
            Alumni("Adre", "Sofia", 1997, "0895623362", "adreD@gmail.com", 5.0),
            Alumni()};

        std::vector<NonTeachingStaff> vNts = {
            NonTeachingStaff("Adre", "Sofia", 1997, "0895623362",
                             "adreD@gmail.com", 700.0, 4, "Secretary"),
            NonTeachingStaff()};

        std::vector<TeachingStaff*> vTs = {
            new HomeRoomTeacher(),
            new SubjectTeacher("Adre", "Sofia", 1997, "0895623362",
                               "adreD@gmail.com", 700.0, 4, "Dean",
                               "Geography")};

        fpms.setAlumnus(vAl);
        fpms.setNonTeachers(vNts);
        fpms.setTeachers(vTs);
        fpms.setStudents(vSt);
    }

    SUBCASE("Testing copy constructor.")
    {
        School fpms;
        fpms.setName("First private mathematical school");
        fpms.setFoundationYear(2005);

        CHECK(strcmp(fpms.getName(), "First private mathematical school") == 0);
        CHECK(fpms.getFoundationYear() == 2005);

        std::vector<Student> vSt = {Student("Adre", "Sofia", 1997, "0895623362",
                                            "adreD@gmail.com", 5.0, 5, "1B"),
                                    Student()};

        std::vector<Alumni> vAl = {
            Alumni("Adre", "Sofia", 1997, "0895623362", "adreD@gmail.com", 5.0),
            Alumni()};

        std::vector<NonTeachingStaff> vNts = {
            NonTeachingStaff("Adre", "Sofia", 1997, "0895623362",
                             "adreD@gmail.com", 700.0, 4, "Secretary"),
            NonTeachingStaff()};

        std::vector<TeachingStaff*> vTs = {
            new HomeRoomTeacher(),
            new SubjectTeacher("Adre", "Sofia", 1997, "0895623362",
                               "adreD@gmail.com", 700.0, 4, "Dean",
                               "Geography")};

        fpms.setAlumnus(vAl);
        fpms.setNonTeachers(vNts);
        fpms.setTeachers(vTs);
        fpms.setStudents(vSt);

        School copyFpms(fpms);
        CHECK(strcmp(copyFpms.getName(), "First private mathematical school") ==
              0);
        CHECK(copyFpms.getFoundationYear() == 2005);
    }

    SUBCASE("Testing operator=.")
    {
        School fpms;
        fpms.setName("First private mathematical school");
        fpms.setFoundationYear(2005);

        CHECK(strcmp(fpms.getName(), "First private mathematical school") == 0);
        CHECK(fpms.getFoundationYear() == 2005);

        std::vector<Student> vSt = {Student("Adre", "Sofia", 1997, "0895623362",
                                            "adreD@gmail.com", 5.0, 5, "1B"),
                                    Student()};

        std::vector<Alumni> vAl = {
            Alumni("Adre", "Sofia", 1997, "0895623362", "adreD@gmail.com", 5.0),
            Alumni()};

        std::vector<NonTeachingStaff> vNts = {
            NonTeachingStaff("Adre", "Sofia", 1997, "0895623362",
                             "adreD@gmail.com", 700.0, 4, "Secretary"),
            NonTeachingStaff()};

        std::vector<TeachingStaff*> vTs = {
            new HomeRoomTeacher(),
            new SubjectTeacher("Adre", "Sofia", 1997, "0895623362",
                               "adreD@gmail.com", 700.0, 4, "Dean",
                               "Geography")};

        fpms.setAlumnus(vAl);
        fpms.setNonTeachers(vNts);
        fpms.setTeachers(vTs);
        fpms.setStudents(vSt);

        School copyFpms;
        copyFpms = fpms;
        CHECK(strcmp(copyFpms.getName(), "First private mathematical school") ==
              0);
        CHECK(copyFpms.getFoundationYear() == 2005);
    }

    SUBCASE("Testing arrays.")
    {
        School fpms;
        fpms.setName("First private mathematical school");
        fpms.setFoundationYear(2005);

        CHECK(strcmp(fpms.getName(), "First private mathematical school") == 0);
        CHECK(fpms.getFoundationYear() == 2005);

        std::vector<Student> vSt = {Student("Adre", "Sofia", 1997, "0895623362",
                                            "adreD@gmail.com", 5.0, 5, "1B"),
                                    Student()};

        std::vector<Alumni> vAl = {
            Alumni("Adre", "Sofia", 1997, "0895623362", "adreD@gmail.com", 5.0),
            Alumni()};

        std::vector<NonTeachingStaff> vNts = {
            NonTeachingStaff("Adre", "Sofia", 1997, "0895623362",
                             "adreD@gmail.com", 700.0, 4, "Secretary"),
            NonTeachingStaff()};

        std::vector<TeachingStaff*> vTs = {
            new HomeRoomTeacher(),
            new SubjectTeacher("Adre", "Sofia", 1997, "0895623362",
                               "adreD@gmail.com", 700.0, 4, "Dean",
                               "Geography")};

        fpms.setAlumnus(vAl);
        fpms.setNonTeachers(vNts);
        fpms.setTeachers(vTs);
        fpms.setStudents(vSt);

        auto alumnusArray = fpms.getAlumnus();
        auto nonTeachersArray = fpms.getNonTeachers();
        auto teachersArray = fpms.getTeachers();
        auto studentsArray = fpms.getStudents();

        int lengthAlumusArray = alumnusArray.size();
        for (int i = 0; i < lengthAlumusArray; i++)
        {
            CHECK(vAl[i] == alumnusArray[i]);
        }

        int lengthNonTeachersArray = nonTeachersArray.size();
        for (int i = 0; i < lengthNonTeachersArray; i++)
        {
            CHECK(vNts[i] == nonTeachersArray[i]);
        }

        int lengthTeachersArray = teachersArray.size();
        for (int i = 0; i < lengthTeachersArray; i++)
        {
            CHECK(vTs[i] == teachersArray[i]);
        }

        int lengthStudentsArray = studentsArray.size();
        for (int i = 0; i < lengthStudentsArray; i++)
        {
            CHECK(vSt[i] == studentsArray[i]);
        }

        fpms.removeTeacher("Adre");
        teachersArray = fpms.getTeachers();
        CHECK(teachersArray.size() == 1);
        CHECK(*teachersArray[0] == *vTs[0]);
    }
}