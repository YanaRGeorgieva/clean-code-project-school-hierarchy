#include "./../headers/dynamicMemoryStringArray.h"
#include <cassert>
#include <cstring>

DynamicMemoryStringArray::DynamicMemoryStringArray(const int& capacity)
{
    size = 0;
    this->capacity = (capacity > 0) ? capacity : DEFAULT_ARRAY_CAPACITY;
    array = new char*[this->capacity];
    assert(array != nullptr);
}

DynamicMemoryStringArray::DynamicMemoryStringArray(
    const DynamicMemoryStringArray& otherArray)
{
    copyDynamicMemoryStringArray(otherArray);
}

DynamicMemoryStringArray::~DynamicMemoryStringArray()
{
    deleteDynamicMemoryStringArray();
}

DynamicMemoryStringArray& DynamicMemoryStringArray::operator=(
    const DynamicMemoryStringArray& otherArray)
{
    if (this != &otherArray)
    {
        deleteDynamicMemoryStringArray();
        copyDynamicMemoryStringArray(otherArray);
    }
    return *this;
}

int DynamicMemoryStringArray::getSize() const { return size; }

char*& DynamicMemoryStringArray::operator[](const int& index)
{
    assert(index >= 0 && index < size);
    return array[index];
}

/** @brief Function which adds a specified string to the array.
 *
 * @param element is a C++ string
 */
void DynamicMemoryStringArray::addElement(const char* element)
{
    if (size + 1 > capacity)
    {
        resize(2);
    }
    copyDynamicString(array[size++], element);
}

/** @brief Function which removes a specified string from the array.
 *
 * @param element is a C++ string
 */
void DynamicMemoryStringArray::removeElement(const char* element)
{
    if (size <= 0)
    {
        fprintf(stderr, "Array is empty!\nCannot remove this element!\n");
    }
    int index = findElementIndex(element);
    if (index != -1)
    {
        delete array[index];
        fixPointersAfterRemoveElement(index);
        size--;
        return;
    }
    else
    {
        fprintf(stderr, "Element not found!\n");
        return;
    }
    if (size < capacity / 4)
    {
        resize(-2);
    }
}

int DynamicMemoryStringArray::findElementIndex(const char* element) const
{
    for (int index = 0; index < size; index++)
    {
        if (strcmp(array[index], element) == 0)
        {
            return index;
        }
    }
    return -1;
}

void DynamicMemoryStringArray::copyDynamicMemoryStringArray(
    const DynamicMemoryStringArray& otherArray)
{
    assert(otherArray.size >= 0);
    array = new char*[otherArray.size];
    assert(array != nullptr);
    size = otherArray.size;

    for (int i = 0; i < size; i++)
    {
        copyDynamicString(array[i], otherArray.array[i]);
    }

    capacity = otherArray.capacity;
}

void DynamicMemoryStringArray::deleteDynamicMemoryStringArray()
{
    for (int i = 0; i < size; i++)
    {
        delete array[i];
    }
    delete array;
    size = 0;
    capacity = 0;
}

void DynamicMemoryStringArray::resize(const int& resizeCoefficient)
{
    if (resizeCoefficient > 0)
    {
        capacity *= resizeCoefficient;
    }
    else if (resizeCoefficient < 0)
    {
        capacity /= resizeCoefficient;
    }
    else
    {
        fprintf(stderr,
                "Cannot set capasity to null and loose all information!\n");
        return;
    }

    char** tmp = array;
    array = new char*[capacity];
    assert(array != NULL);
    for (int i = 0; i < size; i++)
    {
        array[i] = tmp[i];
    }
    delete tmp;
}

void DynamicMemoryStringArray::fixPointersAfterRemoveElement(const int& index)
{
    for (int i = index; i < size - 1; i++)
    {
        array[i] = array[i + 1];
    }
    array[size - 1] = nullptr;
}

void DynamicMemoryStringArray::copyDynamicString(char*& copyString,
                                                 const char* originalString)
{
    copyString = new char[strlen(originalString)];
    assert(copyString != NULL);
    strcpy(copyString, originalString);
}