#include "./../headers/school.h"

int main()
{
    School fpms;
    fpms.setName("First private mathematical school");
    fpms.setFoundationYear(2005);

    std::vector<Student> vSt = {Student("Derek", "Sofia", 1997, "0895623362",
                                        "adreD@gmail.com", 5.0, 5, "1B"),
                                Student("Veni")};

    std::vector<Alumni> vAl = {
        Alumni("Maria", "Sofia", 1997, "0895623362", "adreD@gmail.com", 5.0),
        Alumni("Lora")};

    std::vector<NonTeachingStaff> vNts = {
        NonTeachingStaff("Andre", "Sofia", 1997, "0895623362",
                         "adreD@gmail.com", 700.0, 4, "Secretary"),
        NonTeachingStaff("Nona")};

    std::vector<TeachingStaff*> vTs = {
        new HomeRoomTeacher("Hodge"),
        new SubjectTeacher("Adre", "Sofia", 1997, "0895623362",
                           "adreD@gmail.com", 700.0, 4, "Dean", "Geography")};

    fpms.setAlumnus(vAl);
    fpms.setNonTeachers(vNts);
    fpms.setTeachers(vTs);
    fpms.setStudents(vSt);

    auto alumnusArray = fpms.getAlumnus();
    auto nonTeachersArray = fpms.getNonTeachers();
    auto teachersArray = fpms.getTeachers();
    auto studentsArray = fpms.getStudents();

    int lengthAlumusArray = alumnusArray.size();
    printf("Alumnus\n");
    for (int i = 0; i < lengthAlumusArray; i++)
    {
        printf("%s\n", alumnusArray[i].getName());
    }

    int lengthNonTeachersArray = nonTeachersArray.size();
    printf("Non teaching staff\n");
    for (int i = 0; i < lengthNonTeachersArray; i++)
    {
        printf("%s\n", nonTeachersArray[i].getName());
    }

    int lengthTeachersArray = teachersArray.size();
    printf("Teaching staff\n");
    for (int i = 0; i < lengthTeachersArray; i++)
    {
        printf("%s\n", teachersArray[i]->getName());
    }

    int lengthStudentsArray = studentsArray.size();
    printf("Students\n");
    for (int i = 0; i < lengthStudentsArray; i++)
    {
        printf("%s\n", studentsArray[i].getName());
    }

    fpms.removeTeacher("Adre");
    teachersArray = fpms.getTeachers();
    printf("\nLast teacher left in %s found in %d.\n%s\n", fpms.getName(),
           fpms.getFoundationYear(), teachersArray[0]->getName());

    return 0;
}