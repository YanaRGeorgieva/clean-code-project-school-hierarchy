#include "./../headers/nonTeachingStaff.h"
#include <cstring>
#include <stdio.h>

NonTeachingStaff::NonTeachingStaff(const char* name,
                                   const char* address,
                                   const int& birthYear,
                                   const char* telephoneNumber,
                                   const char* email,
                                   const double& monthlySalary,
                                   const int& experience,
                                   const char* job)
    : Employee(name,
               address,
               birthYear,
               telephoneNumber,
               email,
               monthlySalary,
               experience)
{
    this->job = nullptr;
    setJob(job);
}

NonTeachingStaff::NonTeachingStaff(
    const NonTeachingStaff& otherNonTeachingStaff)
    : Employee(otherNonTeachingStaff)
{
    copyNonTeachingStaff(otherNonTeachingStaff);
}

NonTeachingStaff::~NonTeachingStaff() { deleteNonTeachingStaff(); }

NonTeachingStaff& NonTeachingStaff::operator=(
    const NonTeachingStaff& otherNonTeachingStaff)
{
    if (this != &otherNonTeachingStaff)
    {
        Employee::operator=(otherNonTeachingStaff);
        deleteNonTeachingStaff();
        copyNonTeachingStaff(otherNonTeachingStaff);
    }
    return *this;
}

char* NonTeachingStaff::getJob() const { return (char*)job; }

void NonTeachingStaff::setJob(const char* job)
{
    if (strlen(job) == 0 || !isItemInBag(nonTeachnicalStaffJobs, job))
    {
        fprintf(stderr,
                "Error!\nThe length of the new job cannot be 0 or you have "
                "entered an invalid job!\n");
        return;
    }
    if (this->job != nullptr)
    {
        delete this->job;
    }
    copyDynamicString(this->job, job);
}

/** @brief Virtual functions which increases the monthly salary of a nonteaching
 * staff member depending on his/her job.
 */
void NonTeachingStaff::increaseMonthlySalary()
{
    const char* theJob = getJob();
    double result = retrieveMoneyInMapping(nonTeachnicalStaffJobs, theJob);
    if (result < 0.0)
    {
        fprintf(stderr, "Error!\nYou have entered an invalid job!\n");
        return;
    }
    monthlySalary += result;
}

/** @brief Virtual functions which decreases the monthly salary of a nonteaching
 * staff member depending on his/her job.
 */

void NonTeachingStaff::decreaseMonthlySalary()
{
    const char* theJob = getJob();
    double result = retrieveMoneyInMapping(nonTeachnicalStaffJobs, theJob);
    if (result < 0.0)
    {
        fprintf(stderr, "Error!\nYou have entered an invalid job!\n");
        return;
    }
    monthlySalary -= result;
}

void NonTeachingStaff::copyNonTeachingStaff(
    const NonTeachingStaff& otherNonTeachingStaff)
{
    this->job = nullptr;
    setJob(otherNonTeachingStaff.job);
}

void NonTeachingStaff::deleteNonTeachingStaff() { delete job; }