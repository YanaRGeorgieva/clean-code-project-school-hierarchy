#include "./../headers/homeRoomTeacher.h"
#include <cstring>

HomeRoomTeacher::HomeRoomTeacher(const char* name,
                                 const char* address,
                                 const int& birthYear,
                                 const char* telephoneNumber,
                                 const char* email,
                                 const double& monthlySalary,
                                 const int& experience,
                                 const char* schoolRank,
                                 const char* gradeClassTaught)
    : TeachingStaff(name,
                    address,
                    birthYear,
                    telephoneNumber,
                    email,
                    monthlySalary,
                    experience,
                    schoolRank)
{
    this->gradeClassTaught = nullptr;
    setGradeClassTaught(gradeClassTaught);
}

HomeRoomTeacher::HomeRoomTeacher(const HomeRoomTeacher& otherHomeRoomTeacher)
    : TeachingStaff(otherHomeRoomTeacher)
{
    copyHomeRoomTeacher(otherHomeRoomTeacher);
}

HomeRoomTeacher::~HomeRoomTeacher() { deleteHomeRoomTeacher(); }

HomeRoomTeacher& HomeRoomTeacher::operator=(
    const HomeRoomTeacher& otherHomeRoomTeacher)
{
    if (this != &otherHomeRoomTeacher)
    {
        TeachingStaff::operator=(otherHomeRoomTeacher);
        deleteHomeRoomTeacher();
        copyHomeRoomTeacher(otherHomeRoomTeacher);
    }
    return *this;
}

DynamicMemoryStringArray HomeRoomTeacher::getSubjectsTaught() const
{
    return subjectsTaught;
}

char* HomeRoomTeacher::getGradeClassTaught() const
{
    return (char*)gradeClassTaught;
}

/** @brief Virtual function which rewrites the current grade class of the
 * home-room teacher.
 *
 * @param gradeClassTaught some valid grade class in the school.
 */
void HomeRoomTeacher::addGradeClassTaught(const char* gradeClassTaught)
{
    setGradeClassTaught(gradeClassTaught); 
}

/** @brief Virtual function which cannot erase the current grade class of the
 * home-room teacher.
 *
 * @param gradeClassTaught some valid grade class in the school.
 */
void HomeRoomTeacher::removeGradeClassTaught(const char* gradeClassTaught)
{
    fprintf(stderr,
            "You cannot remove the subject.\n%s is a home-room teacher.\n",
            getName());
}

/** @brief Virtual function which add a subject which the home-room teacher
 * teaches to his/her students.
 *
 * @param subjectTaught some valid school subject.
 */
void HomeRoomTeacher::addSubjectTaught(const char* subjectTaught)
{
    if (strlen(subjectTaught) == 0 ||
        !isItemInBag(subjectsInSchool, subjectTaught))
    {
        fprintf(stderr, "Error!\nYou have entered an invalid subject!\n");
        return;
    }
    subjectsTaught.addElement(subjectTaught);
}

/** @brief Virtual function which removes a subject which the home-room teacher
 * no longer teaches to his/her students.
 *
 * @param subjectTaught some valid school subject.
 */
void HomeRoomTeacher::removeSubjectTaught(const char* subjectTaught)
{
    subjectsTaught.removeElement(subjectTaught);
}

void HomeRoomTeacher::copyHomeRoomTeacher(
    const HomeRoomTeacher& otherHomeRoomTeacher)
{
    this->gradeClassTaught = nullptr;
    subjectsTaught = otherHomeRoomTeacher.subjectsTaught;
    setGradeClassTaught(otherHomeRoomTeacher.gradeClassTaught);
}

void HomeRoomTeacher::deleteHomeRoomTeacher() { delete gradeClassTaught; }

void HomeRoomTeacher::setGradeClassTaught(const char* gradeClassTaught)
{
    if (strlen(gradeClassTaught) == 0 ||
        !isItemInBag(gradeClassesInSchool, gradeClassTaught))
    {
        fprintf(stderr, "Error!\nYou have entered an invalid class!\n");
        return;
    }
    if (this->gradeClassTaught != nullptr)
    {
        delete this->gradeClassTaught;
    }
    copyDynamicString(this->gradeClassTaught, gradeClassTaught);
}