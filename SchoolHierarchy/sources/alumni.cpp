#include "./../headers/alumni.h"
#include <cstring>
#include <stdio.h>

Alumni::Alumni(const char* name,
               const char* address,
               const int& birthYear,
               const char* telephoneNumber,
               const char* email,
               const double& diplomaMark)
    : CommunityMember(name, address, birthYear, telephoneNumber, email)
{
    setDiplomaMark(diplomaMark);
}

Alumni::Alumni(const Alumni& otherAlumni) : CommunityMember(otherAlumni)
{
    copyAlumni(otherAlumni);
}

Alumni::~Alumni() { deleteAlumni(); }

Alumni& Alumni::operator=(const Alumni& otherAlumni)
{
    if (this != &otherAlumni)
    {
        CommunityMember::operator=(otherAlumni);
        deleteAlumni();
        copyAlumni(otherAlumni);
    }
    return *this;
}

double Alumni::getDiplomaMark() const { return diplomaMark; }

DynamicMemoryStringArray Alumni::getProfiledSubjects() const
{
    return profiledSubjects;
}

void Alumni::setDiplomaMark(const double& diplomaMark)
{
    if (diplomaMark < 2.0 || diplomaMark > 6.0)
    {
        fprintf(stderr,
                "Error!\nThe diploma mark must be between 2.0 and 6.0!\n");
        return;
    }
    this->diplomaMark = diplomaMark;
}

/** @brief Function which adds a subject as profiled for the alumni.
 *
 * @param subject some valid school subject.
 */
void Alumni::addProfiledSubject(const char* subject)
{
    if (strlen(subject) == 0 || !isItemInBag(subjectsInSchool, subject))
    {
        fprintf(stderr, "Error!\nYou have entered an invalid subject!\n");
        return;
    }
    profiledSubjects.addElement(subject);
}

/** @brief Function which removes a subject as profiled for the alumni.
 *
 * @param subject some valid school subject.
 */
void Alumni::removeProfiledSubject(const char* subject)
{
    profiledSubjects.removeElement(subject);
}

void Alumni::copyAlumni(const Alumni& otherAlumni)
{
    diplomaMark = otherAlumni.diplomaMark;
    profiledSubjects = otherAlumni.profiledSubjects;
}

void Alumni::deleteAlumni()
{
    // automatic destructor of profiledSubjects
}
