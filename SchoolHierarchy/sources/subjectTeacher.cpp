#include "./../headers/subjectTeacher.h"
#include <cstring>

SubjectTeacher::SubjectTeacher(const char* name,
                               const char* address,
                               const int& birthYear,
                               const char* telephoneNumber,
                               const char* email,
                               const double& monthlySalary,
                               const int& experience,
                               const char* schoolRank,
                               const char* subjectTaught)
    : TeachingStaff(name,
                    address,
                    birthYear,
                    telephoneNumber,
                    email,
                    monthlySalary,
                    experience,
                    schoolRank)
{
    this->subjectTaught = nullptr;
    setSubjectTaught(subjectTaught);
}

SubjectTeacher::SubjectTeacher(const SubjectTeacher& otherSubjectTeacher)
    : TeachingStaff(otherSubjectTeacher)
{

    copySubjectTeacher(otherSubjectTeacher);
}

SubjectTeacher::~SubjectTeacher() { deleteSubjectTeacher(); }

SubjectTeacher& SubjectTeacher::operator=(
    const SubjectTeacher& otherSubjectTeacher)
{
    if (this != &otherSubjectTeacher)
    {
        TeachingStaff::operator=(otherSubjectTeacher);
        deleteSubjectTeacher();
        copySubjectTeacher(otherSubjectTeacher);
    }
    return *this;
}

DynamicMemoryStringArray SubjectTeacher::getGradeClassesTaught() const
{
    return gradeClassesTaught;
}

char* SubjectTeacher::getSubjectTaught() const { return (char*)subjectTaught; }

/** @brief Virtual function which add a grade class to whome the subject
 * teacher teaches his/her subject.
 *
 * @param subjectTaught some valid school subject.
 */
void SubjectTeacher::addGradeClassTaught(const char* gradeClassTaught)
{
    if (strlen(gradeClassTaught) == 0 ||
        !isItemInBag(gradeClassesInSchool, gradeClassTaught))
    {
        fprintf(stderr, "Error!\nYou have entered an invalid class!\n");
        return;
    }
    gradeClassesTaught.addElement(gradeClassTaught);
}

/** @brief Virtual function which add a grade class to whome the subject
 * teacher no longer teaches his/her subject.
 *
 * @param subjectTaught some valid school subject.
 */
void SubjectTeacher::removeGradeClassTaught(const char* gradeClassTaught)
{
    gradeClassesTaught.removeElement(gradeClassTaught);
}

/** @brief Virtual function which rewrites the current subject of the
 * subject teacher.
 *
 * @param subjectTaught some valid school subject.
 */
void SubjectTeacher::addSubjectTaught(const char* subjectTaught)
{
    setSubjectTaught(subjectTaught);
}

/** @brief Virtual function which cannot erase the current subject of the
 * subject teacher.
 *
 * @param subjectTaught some valid school subject.
 */
void SubjectTeacher::removeSubjectTaught(const char* subjectTaught)
{
    fprintf(stderr,
            "You cannot remove the subject.\n%s is a subject teacher.\n",
            getName());
}

void SubjectTeacher::copySubjectTeacher(
    const SubjectTeacher& otherSubjectTeacher)
{
    this->subjectTaught = nullptr;
    gradeClassesTaught = otherSubjectTeacher.gradeClassesTaught;
    setSubjectTaught(otherSubjectTeacher.subjectTaught);
}

void SubjectTeacher::deleteSubjectTeacher() { delete subjectTaught; }

void SubjectTeacher::setSubjectTaught(const char* subjectTaught)
{
    if (strlen(subjectTaught) == 0 ||
        !isItemInBag(subjectsInSchool, subjectTaught))
    {
        fprintf(stderr, "Error!\nYou have entered an invalid subject!\n");
        return;
    }
    if (this->subjectTaught != nullptr)
        delete this->subjectTaught;
    copyDynamicString(this->subjectTaught, subjectTaught);
}
