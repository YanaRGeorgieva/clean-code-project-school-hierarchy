#include "./../headers/teachingStaff.h"
#include <cstring>

TeachingStaff::TeachingStaff(const char* name,
                             const char* address,
                             const int& birthYear,
                             const char* telephoneNumber,
                             const char* email,
                             const double& monthlySalary,
                             const int& experience,
                             const char* schoolRank)
    : Employee(name,
               address,
               birthYear,
               telephoneNumber,
               email,
               monthlySalary,
               experience)
{
    this->schoolRank = nullptr;
    setSchoolRank(schoolRank);
}

TeachingStaff::TeachingStaff(const TeachingStaff& otherTeachingStaff)
    : Employee(otherTeachingStaff)
{
    copyTeachingStaff(otherTeachingStaff);
}

TeachingStaff::~TeachingStaff() { deleteTeachingStaff(); }

TeachingStaff& TeachingStaff::operator=(const TeachingStaff& otherTeachingStaff)
{
    if (this != &otherTeachingStaff)
    {
        Employee::operator=(otherTeachingStaff);
        deleteTeachingStaff();
        copyTeachingStaff(otherTeachingStaff);
    }
    return *this;
}

char* TeachingStaff::getSchoolRank() const { return (char*)schoolRank; }

void TeachingStaff::setSchoolRank(const char* schoolRank)
{
    if (strlen(schoolRank) == 0 || !isItemInBag(schoolRanks, schoolRank))
    {
        fprintf(
            stderr,
            "Error!\nThe length of the new school rank cannot be 0 or you have "
            "entered an invalid school rank!\n");
        return;
    }
    if (this->schoolRank != nullptr)
        delete this->schoolRank;
    copyDynamicString(this->schoolRank, schoolRank);
}

/** @brief Function which increases the monthly salary of the
 * teacher depending on his/her rank.
 */
void TeachingStaff::increaseMonthlySalary()
{
    const char* rank = getSchoolRank();
    double result = retrieveMoneyInMapping(schoolRanks, rank);
    if (result < 0.0)
    {
        fprintf(stderr, "Error!\nYou have entered an invalid rank!\n");
        return;
    }
    monthlySalary += result;
}

/** @brief Function which decreases the monthly salary of the
 * teacher depending on his/her rank.
 */
void TeachingStaff::decreaseMonthlySalary()
{
    const char* rank = getSchoolRank();
    double result = retrieveMoneyInMapping(schoolRanks, rank);
    if (result < 0.0)
    {
        fprintf(stderr, "Error!\nYou have entered an invalid rank!\n");
        return;
    }
    monthlySalary -= result;
}

void TeachingStaff::copyTeachingStaff(const TeachingStaff& otherTeachingStaff)
{
    this->schoolRank = nullptr;
    setSchoolRank(otherTeachingStaff.schoolRank);
}

void TeachingStaff::deleteTeachingStaff() { delete schoolRank; }
