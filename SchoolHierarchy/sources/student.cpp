#include "./../headers/student.h"
#include <cstring>
#include <stdio.h>

Student::Student(const char* name,
                 const char* address,
                 const int& birthGrade,
                 const char* telephoneNumber,
                 const char* email,
                 const double& averageMark,
                 const int& classNumber,
                 const char* gradeClass)
    : CommunityMember(name, address, birthGrade, telephoneNumber, email)
{
    this->gradeClass = nullptr;
    setAverageMark(averageMark);
    setClassNumber(classNumber);
    setGradeClass(gradeClass);
}

Student::Student(const Student& otherStudent) : CommunityMember(otherStudent)
{
    copyStudent(otherStudent);
}

Student::~Student() { deleteStudent(); }

Student& Student::operator=(const Student& otherStudent)
{
    if (this != &otherStudent)
    {
        CommunityMember::operator=(otherStudent);
        deleteStudent();
        copyStudent(otherStudent);
    }
    return *this;
}

double Student::getAverageMark() const { return averageMark; }

DynamicMemoryStringArray Student::getProfiledSubjects() const
{
    return profiledSubjects;
}

int Student::getClassNumber() const { return classNumber; }

char* Student::getGradeClass() const { return (char*)gradeClass; }

void Student::setAverageMark(const double& averageMark)
{
    if (averageMark < 2.0 || averageMark > 6.0)
    {
        fprintf(stderr,
                "Error!\nThe diploma mark must be between 2.0 and 6.0!\n");
        return;
    }
    this->averageMark = averageMark;
}

void Student::setClassNumber(const int& classNumber)
{
    if (classNumber <= 0)
    {
        fprintf(stderr,
                "Error!\nThe class number must be a positive number!\n");
        return;
    }
    this->classNumber = classNumber;
}
void Student::setGradeClass(const char* gradeClass)
{
    if (strlen(gradeClass) == 0 ||
        !isItemInBag(gradeClassesInSchool, gradeClass))
    {
        fprintf(stderr, "Error!\nYou have entered an invalid class!\n");
        return;
    }
    if (this->gradeClass != nullptr)
    {
        delete this->gradeClass;
    }
    copyDynamicString(this->gradeClass, gradeClass);
}

/** @brief Function which adds a subject as profiled for the student.
 *
 * @param subject some valid school subject.
 */
void Student::addProfiledSubject(const char* subject)
{
    if (strlen(subject) == 0 || !isItemInBag(subjectsInSchool, subject))
    {
        fprintf(stderr, "Error!\nYou have entered an invalid subject!\n");
        return;
    }
    profiledSubjects.addElement(subject);
}

/** @brief Function which removes a subject as profiled for the student.
 *
 * @param subject some valid school subject.
 */
void Student::removeProfiledSubject(const char* subject)
{
    profiledSubjects.removeElement(subject);
}

void Student::copyStudent(const Student& otherStudent)
{
    this->gradeClass = nullptr;
    setAverageMark(otherStudent.averageMark);
    setClassNumber(otherStudent.classNumber);
    setGradeClass(otherStudent.gradeClass);
    profiledSubjects = otherStudent.profiledSubjects;
}

void Student::deleteStudent()
{
    // automatic destructor of profiledSubjects
    averageMark = 0.0;
    classNumber = 0;
    delete gradeClass;
}
