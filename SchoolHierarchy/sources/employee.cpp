#include "./../headers/employee.h"
#include <stdio.h>

Employee::Employee(const char* name,
                   const char* address,
                   const int& birthYear,
                   const char* telephoneNumber,
                   const char* email,
                   const double& monthlySalary,
                   const int& experience)
    : CommunityMember(name, address, birthYear, telephoneNumber, email)
{
    setMonthlySalary(monthlySalary);
    setExperience(experience);
}

Employee::Employee(const Employee& otherEmployee)
    : CommunityMember(otherEmployee)
{
    copyEmployee(otherEmployee);
}

Employee::~Employee() { deleteEmployee(); }

Employee& Employee::operator=(const Employee& otherEmployee)
{
    if (this != &otherEmployee)
    {
        CommunityMember::operator=(otherEmployee);
        deleteEmployee();
        copyEmployee(otherEmployee);
    }
    return *this;
}

int Employee::getExpereince() const { return experience; }

double Employee::getMonthlySalary() const { return monthlySalary; }

void Employee::setExperience(const int& experience)
{
    if (experience < 0)
    {
        fprintf(stderr,
                "Error!\nThe experience must be a non-negative number!\n");
        return;
    }
    this->experience = experience;
}

void Employee::setMonthlySalary(const double& monthlySalary)
{
    if (monthlySalary <= 0.0)
    {
        fprintf(stderr,
                "Error!\nThe monthly salary must be a positive number!\n");
        return;
    }
    this->monthlySalary = monthlySalary;
}

void Employee::copyEmployee(const Employee& otherEmployee)
{
    setMonthlySalary(otherEmployee.monthlySalary);
    setExperience(otherEmployee.experience);
}

void Employee::deleteEmployee()
{
    monthlySalary = 0.0;
    experience = 0;
}