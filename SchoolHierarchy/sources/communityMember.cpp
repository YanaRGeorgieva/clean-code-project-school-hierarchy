#include "./../headers/communityMember.h"
#include <cstring>
#include <ctime>
#include <stdio.h>

CommunityMember::CommunityMember(const char* name,
                                 const char* address,
                                 const int& birthYear,
                                 const char* telephoneNumber,
                                 const char* email)
{
    this->name = nullptr;
    this->address = nullptr;
    this->telephoneNumber = nullptr;
    this->email = nullptr;
    setName(name);
    setAddress(address);
    setBirthYear(birthYear);
    setTelephoneNumber(telephoneNumber);
    setEmail(email);
}

CommunityMember::CommunityMember(const CommunityMember& otherCommunityMember)
{
    copyCommunityMember(otherCommunityMember);
}

CommunityMember::~CommunityMember() { deleteCommunityMember(); }

CommunityMember& CommunityMember::operator=(
    const CommunityMember& otherCommunityMember)
{
    if (this != &otherCommunityMember)
    {
        deleteCommunityMember();
        copyCommunityMember(otherCommunityMember);
    }
    return *this;
}

char* CommunityMember::getName() const { return (char*)name; }

char* CommunityMember::getAddress() const { return (char*)address; }

int CommunityMember::getBirthYear() const { return birthYear; }

char* CommunityMember::getTelephoneNumber() const
{
    return (char*)telephoneNumber;
}

char* CommunityMember::getEmail() const { return (char*)email; }

/** @brief Function which calculates age of the CommunityMember.
 *
 * @return The calculated age.
 */
int CommunityMember::calculateAge() const
{
    return getCurrentYear() - birthYear;
}

bool CommunityMember::operator==(
    const CommunityMember& otherCommunityMember) const
{
    return (strcmp(name, otherCommunityMember.name) == 0);
}

void CommunityMember::setName(const char* name)
{
    if (strlen(name) == 0)
    {
        fprintf(stderr, "Error!\nLength of the new name cannot be 0!\n");
        return;
    }
    if (this->name != nullptr)
    {
        delete this->name;
    }

    copyDynamicString(this->name, name);
}

void CommunityMember::setAddress(const char* address)
{
    if (strlen(address) == 0)
    {
        fprintf(stderr, "Error!\nLength of the new address cannot be 0!\n");
        return;
    }
    if (this->address != nullptr)
    {
        delete this->address;
    }
    copyDynamicString(this->address, address);
}

void CommunityMember::setBirthYear(const int& birthYear)
{
    if (birthYear <= 0)
    {
        fprintf(stderr, "Error!\nBirth year must be a positive number!\n");
        return;
    }
    this->birthYear = birthYear;
}

void CommunityMember::setTelephoneNumber(const char* telephoneNumber)
{
    if (strlen(telephoneNumber) == 0)
    {
        fprintf(stderr,
                "Error!\nLength of the new telephone number cannot be 0!\n");
        return;
    }
    if (this->telephoneNumber != nullptr)
    {
        delete this->telephoneNumber;
    }
    copyDynamicString(this->telephoneNumber, telephoneNumber);
}

void CommunityMember::setEmail(const char* email)
{
    if (strlen(email) == 0)
    {
        fprintf(stderr, "Error!\nLength of the new email cannot be 0!\n");
        return;
    }
    if (this->email != nullptr)
    {
        delete this->email;
    }
    copyDynamicString(this->email, email);
}

void CommunityMember::copyDynamicString(char*& copyString,
                                        const char* originalString)
{
    copyString = new char[strlen(originalString)];
    assert(copyString != NULL);
    strcpy(copyString, originalString);
}

bool CommunityMember::isItemInBag(const bag& haystack, const char* needle) const
{
    auto find = std::find_if(
        haystack.begin(), haystack.end(),
        [&needle](const char* item) { return strcmp(needle, item) == 0; });
    return !(find == haystack.end());
}

bool CommunityMember::isItemInBag(const responsabilityToMoneyBoost& haystack,
                                  const char* needle) const
{
    auto find = std::find_if(
        haystack.begin(), haystack.end(),
        [&needle](const std::pair<const char* const, double> item) {
            return strcmp(needle, item.first) == 0;
        });
    return !(find == haystack.end());
}

double CommunityMember::retrieveMoneyInMapping(
    const responsabilityToMoneyBoost& haystack,
    const char* needle) const
{
    auto find = std::find_if(
        haystack.begin(), haystack.end(),
        [&needle](const std::pair<const char* const, double> item) {
            return strcmp(needle, item.first) == 0;
        });
    if (find == haystack.end())
    {
        return -1.0;
    }
    else
    {
        return find->second;
    }
}

void CommunityMember::copyCommunityMember(
    const CommunityMember& otherCommunityMember)
{
    this->name = nullptr;
    this->address = nullptr;
    this->telephoneNumber = nullptr;
    this->email = nullptr;
    setName(otherCommunityMember.name);
    setAddress(otherCommunityMember.address);
    setBirthYear(otherCommunityMember.birthYear);
    setTelephoneNumber(otherCommunityMember.telephoneNumber);
    setEmail(otherCommunityMember.email);
}

void CommunityMember::deleteCommunityMember()
{
    delete name;
    delete address;
    delete telephoneNumber;
    delete email;
    name = nullptr;
    address = nullptr;
    telephoneNumber = nullptr;
    email = nullptr;
}

/** @brief Function which returns the current year.
 *
 * @return The current year.
 */
int CommunityMember::getCurrentYear() const
{
    time_t currentTime = time(NULL);
    tm* localTime = localtime(&currentTime);
    return localTime->tm_year + DEFAULT_START_YEAR;
}
