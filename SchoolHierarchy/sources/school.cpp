#include "./../headers/school.h"
#include <algorithm>
#include <cstring>
#include <vector>

School::School(const char* name, const int& foundationYear)
{
    this->name = nullptr;
    setName(name);
    setFoundationYear(foundationYear);
}

School::School(const School& otherSchool) { copySchool(otherSchool); }

School::~School() { deleteSchool(); }

School& School::operator=(const School& otherSchool)
{
    if (this != &otherSchool)
    {
        deleteSchool();
        copySchool(otherSchool);
    }
    return *this;
}

char* School::getName() const { return (char*)name; }

int School::getFoundationYear() const { return foundationYear; }

std::vector<TeachingStaff*> School::getTeachers() const { return teachers; }

std::vector<NonTeachingStaff> School::getNonTeachers() const
{
    return nonTeachers;
}

std::vector<Student> School::getStudents() const { return students; }

std::vector<Alumni> School::getAlumnus() const { return alumnus; }

void School::setName(const char* name)
{
    if (strlen(name) == 0)
    {
        fprintf(stderr, "Error!\nLength of the new name cannot be 0!\n");
        return;
    }
    if (this->name != nullptr)
    {
        delete this->name;
    }
    copyDynamicString(this->name, name);
}
void School::setFoundationYear(const int& foundationYear)
{
    if (foundationYear <= 0)
    {
        fprintf(stderr, "Error!\nFoundation year must be a positive number!\n");
        return;
    }
    this->foundationYear = foundationYear;
}
void School::setTeachers(const std::vector<TeachingStaff*>& teachers)
{
    this->teachers = teachers;
}

void School::setNonTeachers(const std::vector<NonTeachingStaff>& nonTeachers)
{
    this->nonTeachers = nonTeachers;
}

void School::setStudents(const std::vector<Student>& students)
{
    this->students = students;
}

void School::setAlumnus(const std::vector<Alumni>& alumnus)
{
    this->alumnus = alumnus;
}

/** @brief Function which adds a chosen teacher to the school.
 *
 * @param teacher is a HomeRoomTeache instance.
 */
void School::addTeacher(HomeRoomTeacher teacher)
{
    TeachingStaff* a = &teacher;
    teachers.push_back(a);
}

/** @brief Function which adds a chosen teacher to the school.
 *
 * @param teacher is a SubjectTeacher instance.
 */
void School::addTeacher(SubjectTeacher teacher)
{
    TeachingStaff* a = &teacher;
    teachers.push_back(a);
}

/** @brief Function which removes a chosen teacher from the school.
 *
 * @param teacherName is the name of the teacher to be removed from the
 * school
 */
void School::removeTeacher(const char* teacherName)
{
    auto find =
        std::find_if(teachers.begin(), teachers.end(),
                     [&teacherName](const TeachingStaff* teacher) {
                         return strcmp(teacherName, teacher->getName()) == 0;
                     });
    teachers.erase(find);
}

/** @brief Function which adds a chosen nonteacher member of the staff to the
 * school.
 *
 * @param nonTeacher is a NonTeachingStaff instance.
 */
void School::addNonTeacher(const NonTeachingStaff& nonTeacher)
{
    nonTeachers.push_back(nonTeacher);
}

/** @brief Function which removes a chosen nonteacher member of the staff from
 * the school.
 *
 * @param nonTeacherName is the name of the nonteacher member to be removed from
 * the school
 */
void School::removeNonTeacher(const char* nonTeacherName)
{
    auto find = std::find_if(
        nonTeachers.begin(), nonTeachers.end(),
        [&nonTeacherName](const NonTeachingStaff& nonTeacher) {
            return strcmp(nonTeacherName, nonTeacher.getName()) == 0;
        });
    nonTeachers.erase(find);
}

/** @brief Function which adds a chosen student to the school.
 *
 * @param student is a Student instance.
 */
void School::addStudent(const Student& student) { students.push_back(student); }

/** @brief Function which removes a chosen student from the school.
 *
 * @param studentName is the name of the student to be removed from
 * the school
 */
void School::removeStudent(const char* studentName)
{
    auto find =
        std::find_if(students.begin(), students.end(),
                     [&studentName](const Student& student) {
                         return strcmp(studentName, student.getName()) == 0;
                     });
    students.erase(find);
}

/** @brief Function which adds a chosen alumni to the school.
 *
 * @param alumni is a Alumni instance.
 */
void School::addAlumni(const Alumni& alumni) { alumnus.push_back(alumni); }

/** @brief Function which removes a chosen alimni from the school.
 *
 * @param alumniName is the name of the alimni to be removed from
 * the school
 */
void School::removeAlumni(const char* alumniName)
{
    auto find = std::find_if(
        alumnus.begin(), alumnus.end(), [&alumniName](const Alumni& alumni) {
            return strcmp(alumniName, alumni.getName()) == 0;
        });
    alumnus.erase(find);
}

void School::copySchool(const School& otherSchool)
{
    this->name = nullptr;
    setName(otherSchool.name);
    setFoundationYear(otherSchool.foundationYear);
    this->teachers = teachers;
    this->nonTeachers = nonTeachers;
    this->students = students;
    this->alumnus = alumnus;
}

void School::deleteSchool() { delete name; }

void School::copyDynamicString(char*& copyString, const char* originalString)
{
    copyString = new char[strlen(originalString)];
    assert(copyString != NULL);
    strcpy(copyString, originalString);
}