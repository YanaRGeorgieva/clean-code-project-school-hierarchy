#include "./../headers/constants.h"

/** @brief A set of all the grades in the school with all the classes.
 *
 * Grade in {1, 2, 3,.., 12}
 * Classes in {A, B, C, D, E}
 */
const bag gradeClassesInSchool = {
    "1A",  "1B",  "1C",  "1D",  "1E",  "2A",  "2B",  "2C",  "2D",  "2E",
    "3A",  "3B",  "3C",  "3D",  "3E",  "4A",  "4B",  "4C",  "4D",  "4E",
    "5A",  "5B",  "5C",  "5D",  "5E",  "6A",  "6B",  "6C",  "6D",  "6E",
    "7A",  "7B",  "7C",  "7D",  "7E",  "8A",  "8B",  "8C",  "8D",  "8E",
    "9A",  "9B",  "9C",  "9D",  "9E",  "10A", "10B", "10C", "10D", "10E",
    "11A", "11B", "11C", "11D", "11E", "12A", "12B", "12C", "12D", "12E",
};

/** @brief A set of all the subjects in the school.
 */
const bag subjectsInSchool = {
    "Mathematics", "Biology",   "Bulgarian", "English", "Spanish", "Geography",
    "French",      "Chemistry", "Physics",   "PE",      "Music",   "Painting"};

/** @brief A mapping describing the relation between the nonteaching job
 * reponsability and how it affects the montly salary.
 */
const responsabilityToMoneyBoost nonTeachnicalStaffJobs = {
    {"Guard", 150.0},
    {"Hygenist", 100.0},
    {"Technical support", 200.0},
    {"Secretary", 250.0},
    {"Phycologist", 225.0}};

/** @brief A mapping describing the relation between the teacher ranks
 * reponsability and how it affects the montly salary.
 */
const responsabilityToMoneyBoost schoolRanks = {
    {"Dean", 500.0},
    {"Deputy Dean", 450.0},
    {"Head of the Departments", 400.0},
    {"Senior teacher", 350.0},
    {"Junior teacher", 300.0},
};

/** @brief Constants needed for default arguments of constructors.
 */
const int DEFAULT_BIRTHDAY_YEAR = 1960;
const int DEFAULT_EXPERIENCE = 5;
const int DEFAULT_CLASS_NUMBER = 1;
const int DEFAULT_FOUNDATION_YEAR = 1900;
const int DEFAULT_START_YEAR = 1900;
const int DEFAULT_ARRAY_CAPACITY = 20;
const double DEFAULT_MONTLY_SALARY = 100.0;
const double DEFAULT_DIPLOMA_MARK = 4.0;
const double DEFAULT_AVERAGE_MARK = 4.0;

const char* DEFAULT_NAME = "Petar Ivanov";
const char* DEFAULT_ADDRESS = "Sofia, ul. Oborishte 8";
const char* DEFAULT_TELEPHONE_NUMBER = "+359887778907";
const char* DEFAULT_EMAIL = "example@smth.org";
const char* DEFAULT_SCHOOL_RANK = "Junior teacher";
const char* DEFAULT_GRADE_CLASS_TAUGHT = "1A";
const char* DEFAULT_GRADE_CLASS = "1A";
const char* DEFAULT_SUBJECT_TAUGHT = "Mathematics";
const char* DEFAULT_JOB = "Guard";
const char* DEFAULT_SCHOOL_NAME = "18 William Gradston";
