var classAlumni =
[
    [ "Alumni", "classAlumni.html#a9f72cca96f44f77e0b81aaa0b82b7e56", null ],
    [ "Alumni", "classAlumni.html#aefa17ce85978a4b6931a768398e21a49", null ],
    [ "~Alumni", "classAlumni.html#aa2ed258cda313fa74fc6af55e8ed089d", null ],
    [ "addProfiledSubject", "classAlumni.html#ae7638884b9663d0bd5e3e88889716399", null ],
    [ "getDiplomaMark", "classAlumni.html#aeb6f4f0fb58436aa97c354660a3dcf81", null ],
    [ "getProfiledSubjects", "classAlumni.html#a64a749f2669b576579f4cb77a6e3df7b", null ],
    [ "operator=", "classAlumni.html#a257b696da970fcbfa1fbf777933c10a3", null ],
    [ "removeProfiledSubject", "classAlumni.html#a7e516a543771a83674e9677d74caddcb", null ],
    [ "setDiplomaMark", "classAlumni.html#ab60acc508bf001713ba9c88de56d2456", null ]
];