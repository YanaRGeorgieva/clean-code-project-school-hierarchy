var classCommunityMember =
[
    [ "CommunityMember", "classCommunityMember.html#a9487f77733965cd9da0740c5f4295a1e", null ],
    [ "CommunityMember", "classCommunityMember.html#abbfdeda0845f95346df0fd0915e23c45", null ],
    [ "~CommunityMember", "classCommunityMember.html#a9dd38188b4410aa6efe28ca04bb9731d", null ],
    [ "calculateAge", "classCommunityMember.html#a30a10db25866bc9619002fa023620ea1", null ],
    [ "copyDynamicString", "classCommunityMember.html#ad2aa8096f70efcc035cc6ac81f240178", null ],
    [ "getAddress", "classCommunityMember.html#a89414c77ab24273051d8deb09ce2e8ea", null ],
    [ "getBirthYear", "classCommunityMember.html#aa19a3afd37b7e44f508df63c699868c3", null ],
    [ "getEmail", "classCommunityMember.html#a271146cabcefa197cbde05fb43311f7c", null ],
    [ "getName", "classCommunityMember.html#abd218e9539f57ede823623c3f38579af", null ],
    [ "getTelephoneNumber", "classCommunityMember.html#a841dc4a033f6c45e368d176fda860854", null ],
    [ "isItemInBag", "classCommunityMember.html#a3604579dea82676e465307d1d6930660", null ],
    [ "isItemInBag", "classCommunityMember.html#a8d112488fa82935759570099db1d56b1", null ],
    [ "operator=", "classCommunityMember.html#a508a6d981f1bd5573e25898771464ec3", null ],
    [ "operator==", "classCommunityMember.html#a5dd572bca8926e8aacb1cf26cc16dc64", null ],
    [ "retrieveMoneyInMapping", "classCommunityMember.html#aa882df8348cc1ad1223114861f803154", null ],
    [ "setAddress", "classCommunityMember.html#ae0ccd8240f29b9457639efaa31a3cb3c", null ],
    [ "setBirthYear", "classCommunityMember.html#ab665293ed7b1b2afd2d3ce4ba7920cb7", null ],
    [ "setEmail", "classCommunityMember.html#aa69281662ea69eea185ac72dcc475723", null ],
    [ "setName", "classCommunityMember.html#aa3ada1c09fb6e15a1505f9a9641be4c1", null ],
    [ "setTelephoneNumber", "classCommunityMember.html#a19bdd8f88158097745147d3cc6bbd0dd", null ]
];