var classdoctest_1_1Approx =
[
    [ "Approx", "classdoctest_1_1Approx.html#a86f0d1b44c1cf095697f23ccdab00802", null ],
    [ "epsilon", "classdoctest_1_1Approx.html#af8df6b0af00fd875e5b6a0c30b86f636", null ],
    [ "operator()", "classdoctest_1_1Approx.html#aae907c5ea1c4ac94e134db9e35da7dce", null ],
    [ "scale", "classdoctest_1_1Approx.html#a62185fd4c09a63dab61bd893574d8473", null ],
    [ "toString", "classdoctest_1_1Approx.html#ac6b20d91b1dcb101423bbea532d532bc", null ],
    [ "operator!=", "classdoctest_1_1Approx.html#a83b3763569a7ecc143c335b630be0e47", null ],
    [ "operator!=", "classdoctest_1_1Approx.html#a7497ef839f8026cc0edd6269a80f3e09", null ],
    [ "operator<", "classdoctest_1_1Approx.html#a39dd697cd13dbe8c527908940de08cae", null ],
    [ "operator<", "classdoctest_1_1Approx.html#aa2776749a39bd17e2e4c5bb81b501aef", null ],
    [ "operator<=", "classdoctest_1_1Approx.html#aa2bfad80c8c138eac1f0b56910a7d3f2", null ],
    [ "operator<=", "classdoctest_1_1Approx.html#a75c9382b61421ffab3559c3506182d8f", null ],
    [ "operator==", "classdoctest_1_1Approx.html#afbe599e8833aef58620a7185dd14531e", null ],
    [ "operator==", "classdoctest_1_1Approx.html#a35999631e6cef569f9da9f3fa910db22", null ],
    [ "operator>", "classdoctest_1_1Approx.html#a2ff21c117188b622095dfb3b3a20d80e", null ],
    [ "operator>", "classdoctest_1_1Approx.html#a15213e0c7da2a9e7363601aefbd9c259", null ],
    [ "operator>=", "classdoctest_1_1Approx.html#a4e60095c615a0e6bdd6e8663cd24090b", null ],
    [ "operator>=", "classdoctest_1_1Approx.html#adaba11ee9aabb4d51d4855f09aa7f7df", null ]
];