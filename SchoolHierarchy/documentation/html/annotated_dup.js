var annotated_dup =
[
    [ "doctest", null, [
      [ "detail", null, [
        [ "has_insertion_operator_impl", null, [
          [ "any_t", "structdoctest_1_1detail_1_1has__insertion__operator__impl_1_1any__t.html", "structdoctest_1_1detail_1_1has__insertion__operator__impl_1_1any__t" ],
          [ "has_insertion_operator", "structdoctest_1_1detail_1_1has__insertion__operator__impl_1_1has__insertion__operator.html", null ]
        ] ],
        [ "static_assert_impl", null, [
          [ "StaticAssertion", "structdoctest_1_1detail_1_1static__assert__impl_1_1StaticAssertion.html", null ],
          [ "StaticAssertion< true >", "structdoctest_1_1detail_1_1static__assert__impl_1_1StaticAssertion_3_01true_01_4.html", null ],
          [ "StaticAssertionTest", "structdoctest_1_1detail_1_1static__assert__impl_1_1StaticAssertionTest.html", null ]
        ] ],
        [ "can_use_op", "structdoctest_1_1detail_1_1can__use__op.html", null ],
        [ "ContextBuilder", "classdoctest_1_1detail_1_1ContextBuilder.html", "classdoctest_1_1detail_1_1ContextBuilder" ],
        [ "ContextScope", "classdoctest_1_1detail_1_1ContextScope.html", "classdoctest_1_1detail_1_1ContextScope" ],
        [ "decay_array", "structdoctest_1_1detail_1_1decay__array.html", "structdoctest_1_1detail_1_1decay__array" ],
        [ "decay_array< T[]>", "structdoctest_1_1detail_1_1decay__array_3_01T[]_4.html", "structdoctest_1_1detail_1_1decay__array_3_01T[]_4" ],
        [ "decay_array< T[N]>", "structdoctest_1_1detail_1_1decay__array_3_01T[N]_4.html", "structdoctest_1_1detail_1_1decay__array_3_01T[N]_4" ],
        [ "deferred_false", "structdoctest_1_1detail_1_1deferred__false.html", null ],
        [ "enable_if", "structdoctest_1_1detail_1_1enable__if.html", null ],
        [ "enable_if< true, TYPE >", "structdoctest_1_1detail_1_1enable__if_3_01true_00_01TYPE_01_4.html", "structdoctest_1_1detail_1_1enable__if_3_01true_00_01TYPE_01_4" ],
        [ "ExceptionTranslator", "classdoctest_1_1detail_1_1ExceptionTranslator.html", "classdoctest_1_1detail_1_1ExceptionTranslator" ],
        [ "Expression_lhs", "structdoctest_1_1detail_1_1Expression__lhs.html", "structdoctest_1_1detail_1_1Expression__lhs" ],
        [ "ExpressionDecomposer", "structdoctest_1_1detail_1_1ExpressionDecomposer.html", "structdoctest_1_1detail_1_1ExpressionDecomposer" ],
        [ "ForEachType", "structdoctest_1_1detail_1_1ForEachType.html", null ],
        [ "ForEachType< Typelist< Head, NullType >, Callable >", "structdoctest_1_1detail_1_1ForEachType_3_01Typelist_3_01Head_00_01NullType_01_4_00_01Callable_01_4.html", "structdoctest_1_1detail_1_1ForEachType_3_01Typelist_3_01Head_00_01NullType_01_4_00_01Callable_01_4" ],
        [ "ForEachType< Typelist< Head, Tail >, Callable >", "structdoctest_1_1detail_1_1ForEachType_3_01Typelist_3_01Head_00_01Tail_01_4_00_01Callable_01_4.html", "structdoctest_1_1detail_1_1ForEachType_3_01Typelist_3_01Head_00_01Tail_01_4_00_01Callable_01_4" ],
        [ "has_insertion_operator", "structdoctest_1_1detail_1_1has__insertion__operator.html", null ],
        [ "IContextScope", "structdoctest_1_1detail_1_1IContextScope.html", "structdoctest_1_1detail_1_1IContextScope" ],
        [ "IExceptionTranslator", "structdoctest_1_1detail_1_1IExceptionTranslator.html", "structdoctest_1_1detail_1_1IExceptionTranslator" ],
        [ "MessageBuilder", "classdoctest_1_1detail_1_1MessageBuilder.html", "classdoctest_1_1detail_1_1MessageBuilder" ],
        [ "not_char_pointer", "structdoctest_1_1detail_1_1not__char__pointer.html", "structdoctest_1_1detail_1_1not__char__pointer" ],
        [ "not_char_pointer< char * >", "structdoctest_1_1detail_1_1not__char__pointer_3_01char_01_5_01_4.html", "structdoctest_1_1detail_1_1not__char__pointer_3_01char_01_5_01_4" ],
        [ "not_char_pointer< const char * >", "structdoctest_1_1detail_1_1not__char__pointer_3_01const_01char_01_5_01_4.html", "structdoctest_1_1detail_1_1not__char__pointer_3_01const_01char_01_5_01_4" ],
        [ "NullType", "classdoctest_1_1detail_1_1NullType.html", null ],
        [ "RelationalComparator", "structdoctest_1_1detail_1_1RelationalComparator.html", "structdoctest_1_1detail_1_1RelationalComparator" ],
        [ "RelationalComparator< 0, L, R >", "structdoctest_1_1detail_1_1RelationalComparator_3_010_00_01L_00_01R_01_4.html", "structdoctest_1_1detail_1_1RelationalComparator_3_010_00_01L_00_01R_01_4" ],
        [ "RelationalComparator< 1, L, R >", "structdoctest_1_1detail_1_1RelationalComparator_3_011_00_01L_00_01R_01_4.html", "structdoctest_1_1detail_1_1RelationalComparator_3_011_00_01L_00_01R_01_4" ],
        [ "RelationalComparator< 2, L, R >", "structdoctest_1_1detail_1_1RelationalComparator_3_012_00_01L_00_01R_01_4.html", "structdoctest_1_1detail_1_1RelationalComparator_3_012_00_01L_00_01R_01_4" ],
        [ "RelationalComparator< 3, L, R >", "structdoctest_1_1detail_1_1RelationalComparator_3_013_00_01L_00_01R_01_4.html", "structdoctest_1_1detail_1_1RelationalComparator_3_013_00_01L_00_01R_01_4" ],
        [ "RelationalComparator< 4, L, R >", "structdoctest_1_1detail_1_1RelationalComparator_3_014_00_01L_00_01R_01_4.html", "structdoctest_1_1detail_1_1RelationalComparator_3_014_00_01L_00_01R_01_4" ],
        [ "RelationalComparator< 5, L, R >", "structdoctest_1_1detail_1_1RelationalComparator_3_015_00_01L_00_01R_01_4.html", "structdoctest_1_1detail_1_1RelationalComparator_3_015_00_01L_00_01R_01_4" ],
        [ "Result", "structdoctest_1_1detail_1_1Result.html", "structdoctest_1_1detail_1_1Result" ],
        [ "ResultBuilder", "structdoctest_1_1detail_1_1ResultBuilder.html", "structdoctest_1_1detail_1_1ResultBuilder" ],
        [ "StringMakerBase", "structdoctest_1_1detail_1_1StringMakerBase.html", null ],
        [ "StringMakerBase< true >", "structdoctest_1_1detail_1_1StringMakerBase_3_01true_01_4.html", null ],
        [ "StringStream", "structdoctest_1_1detail_1_1StringStream.html", null ],
        [ "StringStreamBase", "structdoctest_1_1detail_1_1StringStreamBase.html", null ],
        [ "StringStreamBase< true >", "structdoctest_1_1detail_1_1StringStreamBase_3_01true_01_4.html", null ],
        [ "Subcase", "structdoctest_1_1detail_1_1Subcase.html", "structdoctest_1_1detail_1_1Subcase" ],
        [ "SubcaseSignature", "structdoctest_1_1detail_1_1SubcaseSignature.html", "structdoctest_1_1detail_1_1SubcaseSignature" ],
        [ "TestAccessibleContextState", "structdoctest_1_1detail_1_1TestAccessibleContextState.html", "structdoctest_1_1detail_1_1TestAccessibleContextState" ],
        [ "TestCase", "structdoctest_1_1detail_1_1TestCase.html", "structdoctest_1_1detail_1_1TestCase" ],
        [ "TestFailureException", "structdoctest_1_1detail_1_1TestFailureException.html", null ],
        [ "TestSuite", "structdoctest_1_1detail_1_1TestSuite.html", "structdoctest_1_1detail_1_1TestSuite" ],
        [ "Typelist", "structdoctest_1_1detail_1_1Typelist.html", "structdoctest_1_1detail_1_1Typelist" ]
      ] ],
      [ "Approx", "classdoctest_1_1Approx.html", "classdoctest_1_1Approx" ],
      [ "Context", "classdoctest_1_1Context.html", "classdoctest_1_1Context" ],
      [ "description", "structdoctest_1_1description.html", "structdoctest_1_1description" ],
      [ "expected_failures", "structdoctest_1_1expected__failures.html", "structdoctest_1_1expected__failures" ],
      [ "may_fail", "structdoctest_1_1may__fail.html", "structdoctest_1_1may__fail" ],
      [ "should_fail", "structdoctest_1_1should__fail.html", "structdoctest_1_1should__fail" ],
      [ "skip", "structdoctest_1_1skip.html", "structdoctest_1_1skip" ],
      [ "String", "classdoctest_1_1String.html", "classdoctest_1_1String" ],
      [ "StringMaker", "structdoctest_1_1StringMaker.html", null ],
      [ "StringMaker< R C::* >", "structdoctest_1_1StringMaker_3_01R_01C_1_1_5_01_4.html", null ],
      [ "StringMaker< T * >", "structdoctest_1_1StringMaker_3_01T_01_5_01_4.html", null ],
      [ "test_suite", "structdoctest_1_1test__suite.html", "structdoctest_1_1test__suite" ],
      [ "timeout", "structdoctest_1_1timeout.html", "structdoctest_1_1timeout" ],
      [ "Types", "structdoctest_1_1Types.html", "structdoctest_1_1Types" ],
      [ "Types<>", "structdoctest_1_1Types_3_4.html", "structdoctest_1_1Types_3_4" ]
    ] ],
    [ "Alumni", "classAlumni.html", "classAlumni" ],
    [ "CommunityMember", "classCommunityMember.html", "classCommunityMember" ],
    [ "DynamicMemoryStringArray", "classDynamicMemoryStringArray.html", "classDynamicMemoryStringArray" ],
    [ "Employee", "classEmployee.html", "classEmployee" ],
    [ "HomeRoomTeacher", "classHomeRoomTeacher.html", "classHomeRoomTeacher" ],
    [ "NonTeachingStaff", "classNonTeachingStaff.html", "classNonTeachingStaff" ],
    [ "School", "classSchool.html", "classSchool" ],
    [ "Student", "classStudent.html", "classStudent" ],
    [ "SubjectTeacher", "classSubjectTeacher.html", "classSubjectTeacher" ],
    [ "TeachingStaff", "classTeachingStaff.html", "classTeachingStaff" ]
];