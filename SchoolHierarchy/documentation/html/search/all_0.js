var searchData=
[
  ['addalumni',['addAlumni',['../classSchool.html#a89080e1633bee7cf77d3b6ca303e8fd5',1,'School']]],
  ['addelement',['addElement',['../classDynamicMemoryStringArray.html#aa2fd6b3c7c11afb8bf2ede0ab6f6bf4e',1,'DynamicMemoryStringArray']]],
  ['addgradeclasstaught',['addGradeClassTaught',['../classHomeRoomTeacher.html#aacfc81fb0c1492359cdd9f071d122823',1,'HomeRoomTeacher::addGradeClassTaught()'],['../classSubjectTeacher.html#aadf9dede549c97ec80e22485be41fc52',1,'SubjectTeacher::addGradeClassTaught()']]],
  ['addnonteacher',['addNonTeacher',['../classSchool.html#a6418d00b68d6c10fa357b7b784a0600b',1,'School']]],
  ['addprofiledsubject',['addProfiledSubject',['../classAlumni.html#ae7638884b9663d0bd5e3e88889716399',1,'Alumni::addProfiledSubject()'],['../classStudent.html#a3696a9f165d95b042900d67e3cf2ec11',1,'Student::addProfiledSubject()']]],
  ['addstudent',['addStudent',['../classSchool.html#a1814f2b6afdb9dbf6bcce8fc095f1a5d',1,'School']]],
  ['addsubjecttaught',['addSubjectTaught',['../classHomeRoomTeacher.html#a82554dd56f108d9784c229c384da9760',1,'HomeRoomTeacher::addSubjectTaught()'],['../classSubjectTeacher.html#ab88d52e953ee6c6ef9cdf8e14bc102f8',1,'SubjectTeacher::addSubjectTaught()']]],
  ['addteacher',['addTeacher',['../classSchool.html#a457418f43303353fa6f831fa2cb8e9d9',1,'School::addTeacher(HomeRoomTeacher)'],['../classSchool.html#ad26c29c826a56efe41d776aab63fb3a6',1,'School::addTeacher(SubjectTeacher)']]],
  ['alumni',['Alumni',['../classAlumni.html',1,'']]],
  ['any_5ft',['any_t',['../structdoctest_1_1detail_1_1has__insertion__operator__impl_1_1any__t.html',1,'doctest::detail::has_insertion_operator_impl']]],
  ['approx',['Approx',['../classdoctest_1_1Approx.html',1,'doctest']]]
];
