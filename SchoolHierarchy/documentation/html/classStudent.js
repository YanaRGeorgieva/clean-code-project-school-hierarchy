var classStudent =
[
    [ "Student", "classStudent.html#a5358500585052058140a32ad5fc162ea", null ],
    [ "Student", "classStudent.html#a4bc8fcbadabef3f7b81c40536f3713b1", null ],
    [ "~Student", "classStudent.html#a54a8ea060d6cd04222c3a2f89829f105", null ],
    [ "addProfiledSubject", "classStudent.html#a3696a9f165d95b042900d67e3cf2ec11", null ],
    [ "getAverageMark", "classStudent.html#a8e586cab8a8d268d91ae72176d0181dd", null ],
    [ "getClassNumber", "classStudent.html#a2835b2cb47371620ea46af92975f71dc", null ],
    [ "getGradeClass", "classStudent.html#a8a5adc0fb72bc6873b3d365258237071", null ],
    [ "getProfiledSubjects", "classStudent.html#adf57990ec8d8a218c76b1f56127d96ac", null ],
    [ "operator=", "classStudent.html#aa6060eb7193de5bf6a4ae8e4a52bed45", null ],
    [ "removeProfiledSubject", "classStudent.html#a46833bcd101b1bdf9edb0b2ef787ab4c", null ],
    [ "setAverageMark", "classStudent.html#ae6078cfd4583ace65e3716affc6f77d1", null ],
    [ "setClassNumber", "classStudent.html#ad026bf3e7759e23f574bfa034f37b214", null ],
    [ "setGradeClass", "classStudent.html#ad26e2f4021951203b1bb275bc5064812", null ]
];