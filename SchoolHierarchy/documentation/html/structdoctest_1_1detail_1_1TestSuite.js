var structdoctest_1_1detail_1_1TestSuite =
[
    [ "operator *", "structdoctest_1_1detail_1_1TestSuite.html#a35daa88e9478e9218e7933f1840f2c45", null ],
    [ "operator *", "structdoctest_1_1detail_1_1TestSuite.html#a4c57c18169b4829a72d8b0ec4fd87f96", null ],
    [ "m_description", "structdoctest_1_1detail_1_1TestSuite.html#a0458cf84f4f2d308162b26c95a1bbbce", null ],
    [ "m_expected_failures", "structdoctest_1_1detail_1_1TestSuite.html#ab0167ce62046912d83780302cb86adca", null ],
    [ "m_may_fail", "structdoctest_1_1detail_1_1TestSuite.html#aeaf438e6731c002c2447e8e87c46c82b", null ],
    [ "m_should_fail", "structdoctest_1_1detail_1_1TestSuite.html#a3c5953ed157cfc68dfc37cce66fb4103", null ],
    [ "m_skip", "structdoctest_1_1detail_1_1TestSuite.html#a82ecf10ca3db6bff60a087378267caea", null ],
    [ "m_test_suite", "structdoctest_1_1detail_1_1TestSuite.html#ab6260436f6fd52d473c0020ff916753c", null ],
    [ "m_timeout", "structdoctest_1_1detail_1_1TestSuite.html#a430d6e400dd91b9a21c7bb06ede81ec9", null ]
];