FMI: Clean Code 2019 final project
===================
### The project represents a simple OOP implemetation of a school hierarchy system.

To run the Unit tests:
* First build the project with the global Makefile.
* Then go to Tester folder and use the Makefile for the tests.


A doxygen generated diagram of the structure of the hierarchy:
![alt text](images/hierarchy.png)


If you want to see the documentation please follow the [link](SchoolHierarchy/documentation).


You can clone the repo and open it locally.
